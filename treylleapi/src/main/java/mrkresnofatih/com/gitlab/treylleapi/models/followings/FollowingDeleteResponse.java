package mrkresnofatih.com.gitlab.treylleapi.models.followings;

public class FollowingDeleteResponse {
    private String message;

    public FollowingDeleteResponse() {
        this.message = "Successfully deleted following";
    }

    public FollowingDeleteResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
