package mrkresnofatih.com.gitlab.treylleapi.services.ticket;

import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import mrkresnofatih.com.gitlab.treylleapi.configurations.TreylleAwsResourceConfig;
import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketPublishRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketPublishResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class SqsTicketService implements TicketService {
    private final QueueMessagingTemplate queueMessagingTemplate;
    private final Logger logger;
    private final TreylleAwsResourceConfig treylleAwsResourceConfig;

    @Autowired
    public SqsTicketService(QueueMessagingTemplate queueMessagingTemplate, TreylleAwsResourceConfig treylleAwsResourceConfig) {
        this.queueMessagingTemplate = queueMessagingTemplate;
        this.treylleAwsResourceConfig = treylleAwsResourceConfig;
        this.logger = LoggerFactory.getLogger(SqsTicketService.class);
    }

    @Override
    public FuncResponse<TicketPublishResponse> publishTicket(TicketPublishRequest publishRequest) {
        logger.info("start publishTicket w. data: {}", publishRequest.toJsonSerialized());
        try {
            queueMessagingTemplate
                    .send(treylleAwsResourceConfig.getSqsQueueName(), MessageBuilder.withPayload(publishRequest.toJsonSerialized()).build());
            return new FuncResponse<>(new TicketPublishResponse());
        } catch (Exception e) {
            logger.error("failed to publish ticket to sqs");
            return new FuncResponse<>("failed to publish ticket to sqs");
        }
    }
}
