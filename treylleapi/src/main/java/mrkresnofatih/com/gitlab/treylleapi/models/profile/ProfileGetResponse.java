package mrkresnofatih.com.gitlab.treylleapi.models.profile;

public class ProfileGetResponse {
    private String username;
    private String fullName;
    private String avatarUrl;
    private String bio;
    private String wallpaperUrl;
    private String joinedAt;

    public ProfileGetResponse() {
    }

    public ProfileGetResponse(String username, String fullName, String avatarUrl, String bio, String wallpaperUrl, String joinedAt) {
        this.username = username;
        this.fullName = fullName;
        this.avatarUrl = avatarUrl;
        this.bio = bio;
        this.wallpaperUrl = wallpaperUrl;
        this.joinedAt = joinedAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getWallpaperUrl() {
        return wallpaperUrl;
    }

    public void setWallpaperUrl(String wallpaperUrl) {
        this.wallpaperUrl = wallpaperUrl;
    }

    public String getJoinedAt() {
        return joinedAt;
    }

    public void setJoinedAt(String joinedAt) {
        this.joinedAt = joinedAt;
    }
}
