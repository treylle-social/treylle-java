package mrkresnofatih.com.gitlab.treylleapi.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {
    @Value("${treylle.auth.issuer}")
    private String authIssuer;

    @Value("${treylle.auth.secret}")
    private String authSecret;

    @Bean
    public TreylleConfig treylleConfig() {
        return new TreylleConfig(authSecret, authIssuer);
    }
}
