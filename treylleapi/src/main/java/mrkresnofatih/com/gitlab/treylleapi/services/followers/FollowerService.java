package mrkresnofatih.com.gitlab.treylleapi.services.followers;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.*;

public interface FollowerService {
    FuncResponse<FollowerCreateResponse> create(FollowerCreateRequest createRequest);
    FuncResponse<FollowerGetResponse> get(FollowerGetRequest getRequest);
    FuncResponse<FollowerListResponse> list(FollowerListRequest listRequest);
    FuncResponse<FollowerDeleteResponse> delete(FollowerDeleteRequest deleteRequest);
}
