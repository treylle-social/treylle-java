package mrkresnofatih.com.gitlab.treylleapi.models.jwt;

import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

import java.util.Map;
import java.util.Set;

public class JwtValidateRequest extends JsonSerializable {
    private String token;
    private Set<String> extractionClaims;
    private Map<String, String> validationClaims;

    public JwtValidateRequest() {
    }

    public JwtValidateRequest(String token, Set<String> extractionClaims, Map<String, String> validationClaims) {
        this.token = token;
        this.extractionClaims = extractionClaims;
        this.validationClaims = validationClaims;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Set<String> getExtractionClaims() {
        return extractionClaims;
    }

    public void setExtractionClaims(Set<String> extractionClaims) {
        this.extractionClaims = extractionClaims;
    }

    public Map<String, String> getValidationClaims() {
        return validationClaims;
    }

    public void setValidationClaims(Map<String, String> validationClaims) {
        this.validationClaims = validationClaims;
    }
}
