package mrkresnofatih.com.gitlab.treylleapi.models.users;

import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class UserListRequest extends JsonSerializable {
    private String search;
    private String lastResult;
    private int pageSize;

    public UserListRequest() {
    }

    public UserListRequest(String search, String lastResult, int pageSize) {
        this.search = search;
        this.lastResult = lastResult;
        this.pageSize = pageSize;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getLastResult() {
        return lastResult;
    }

    public void setLastResult(String lastResult) {
        this.lastResult = lastResult;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
