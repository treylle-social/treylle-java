package mrkresnofatih.com.gitlab.treylleapi.configurations;

import mrkresnofatih.com.gitlab.treylleapi.models.followers.DynamoFollowerEntityImpl;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.DynamoFollowingEntityImpl;
import mrkresnofatih.com.gitlab.treylleapi.models.users.DynamoUserEntityImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.s3.S3Client;

@Configuration
public class AmazonConfig {
    @Value("${treylle.env.selected}")
    private String environment;

    @Value("${treylle.aws.profile}")
    private String profile;

    @Value("${treylle.aws.region}")
    private String region;

    @Value("${treylle.aws.dynamodb-table}")
    private String dynamodbTableName;

    @Value("${treylle.aws.sqs-queue}")
    private String sqsQueueName;

    AwsCredentialsProvider getAwsCredentialsProvider() {
        if (environment.equals("local")) {
            return ProfileCredentialsProvider.builder()
                    .profileName(profile)
                    .build();
        }
        return EnvironmentVariableCredentialsProvider.create();
    }

    @Bean
    public DynamoDbClient dynamoDbClient() {
        return DynamoDbClient
                .builder()
                .credentialsProvider(getAwsCredentialsProvider())
                .region(Region.of(region))
                .build();
    }

    @Bean
    public S3Client s3Client() {
        return S3Client
                .builder()
                .credentialsProvider(getAwsCredentialsProvider())
                .region(Region.of(region))
                .build();
    }

    @Bean
    public DynamoDbTable<DynamoUserEntityImpl> dynamoUserEntityDynamoDbTable() {
        var db = DynamoDbEnhancedClient.builder()
                .dynamoDbClient(dynamoDbClient())
                .build();
        return db.table(dynamodbTableName, TableSchema.fromBean(DynamoUserEntityImpl.class));
    }

    @Bean
    public DynamoDbTable<DynamoFollowerEntityImpl> dynamoFollowerEntityDynamoDbTable() {
        var db = DynamoDbEnhancedClient.builder()
                .dynamoDbClient(dynamoDbClient())
                .build();
        return db.table(dynamodbTableName, TableSchema.fromBean(DynamoFollowerEntityImpl.class));
    }

    @Bean
    public DynamoDbTable<DynamoFollowingEntityImpl> dynamoFollowingEntityDynamoDbTable() {
        var db = DynamoDbEnhancedClient.builder()
                .dynamoDbClient(dynamoDbClient())
                .build();
        return db.table(dynamodbTableName, TableSchema.fromBean(DynamoFollowingEntityImpl.class));
    }

    @Bean
    public TreylleAwsResourceConfig treylleAwsResourceConfig() {
        return new TreylleAwsResourceConfig(dynamodbTableName, sqsQueueName);
    }
}
