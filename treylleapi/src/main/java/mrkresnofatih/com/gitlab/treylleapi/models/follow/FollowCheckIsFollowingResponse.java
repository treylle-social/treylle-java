package mrkresnofatih.com.gitlab.treylleapi.models.follow;

public class FollowCheckIsFollowingResponse {
    private boolean isFollowing;

    public FollowCheckIsFollowingResponse() {
    }

    public FollowCheckIsFollowingResponse(boolean isFollowing) {
        this.isFollowing = isFollowing;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setFollowing(boolean following) {
        isFollowing = following;
    }
}
