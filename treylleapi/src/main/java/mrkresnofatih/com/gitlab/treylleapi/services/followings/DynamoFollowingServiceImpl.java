package mrkresnofatih.com.gitlab.treylleapi.services.followings;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;

import java.util.ArrayList;
import java.util.Objects;

@Service
public class DynamoFollowingServiceImpl implements FollowingService {
    private final Logger logger;
    private final DynamoDbTable<DynamoFollowingEntityImpl> dynamoFollowingEntityDynamoDbTable;

    @Autowired
    public DynamoFollowingServiceImpl(DynamoDbTable<DynamoFollowingEntityImpl> dynamoFollowingEntityDynamoDbTable) {
        this.dynamoFollowingEntityDynamoDbTable = dynamoFollowingEntityDynamoDbTable;
        this.logger = LoggerFactory.getLogger(DynamoFollowingServiceImpl.class);
    }

    private String getPartitionKey(String username) {
        return String.format("Followings_%s", username);
    }

    private String getSortKey(String followingUsername) {
        return String.format("Following_%s", followingUsername);
    }

    @Override
    public FuncResponse<FollowingCreateResponse> create(FollowingCreateRequest createRequest) {
        logger.info("start create following w. data: {}", createRequest.toJsonSerialized());
        try {
            var newFollowing = new DynamoFollowingEntityImpl();
            newFollowing.setPartitionKey(getPartitionKey(createRequest.getUsername()));
            newFollowing.setSortKey(getSortKey(createRequest.getFollowingUsername()));
            newFollowing.setUsername(createRequest.getUsername());
            newFollowing.setFollowingUsername(createRequest.getFollowingUsername());
            dynamoFollowingEntityDynamoDbTable.putItem(newFollowing);
            var response = new FollowingCreateResponse();
            logger.info("success create following");
            return new FuncResponse<>(response);
        } catch (Exception e) {
            logger.error("create following failed, error: {}", e.getMessage());
            return new FuncResponse<>("failed to create following");
        }
    }

    @Override
    public FuncResponse<FollowingGetResponse> get(FollowingGetRequest getRequest) {
        logger.info("start get following w. data: {}", getRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey(getRequest.getUsername());
            var sortKey = getSortKey(getSortKey(getRequest.getFollowingUsername()));
            var followingKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var followingFound = dynamoFollowingEntityDynamoDbTable.getItem(followingKey);
            if (Objects.isNull(followingFound)) {
                throw new Exception("Following not found");
            }
            var followingGetResponse = new FollowingGetResponse();
            followingGetResponse.setUsername(followingFound.getUsername());
            followingGetResponse.setFollowingUsername(followingFound.getFollowingUsername());
            logger.info("get following success");
            return new FuncResponse<>(followingGetResponse);
        } catch (Exception e) {
            logger.error("get following failed: {}", e.getMessage());
            return new FuncResponse<>("get following failed");
        }
    }

    @Override
    public FuncResponse<FollowingListResponse> list(FollowingListRequest listRequest) {
        logger.info("start list following w. data: {}", listRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey(listRequest.getUsername());
            var sortKey = getSortKey("!");
            if (!Objects.isNull(listRequest.getLastResult())) {
                sortKey = getSortKey(listRequest.getLastResult());
            }
            var startKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var query = QueryEnhancedRequest.builder()
                    .limit(listRequest.getPageSize())
                    .queryConditional(QueryConditional.sortGreaterThan(startKey))
                    .build();
            var result = dynamoFollowingEntityDynamoDbTable
                    .query(query)
                    .items()
                    .iterator();
            var followingList = new ArrayList<FollowingGetResponse>();
            FollowingGetResponse followingDetail;
            while (result.hasNext()) {
                followingDetail = new FollowingGetResponse();
                var record = result.next();
                followingDetail.setUsername(record.getUsername());
                followingDetail.setFollowingUsername(record.getFollowingUsername());
                followingList.add(followingDetail);
            }
            var response = new FollowingListResponse(followingList);
            logger.info("finish list followings");
            return new FuncResponse<>(response);
        } catch (Exception e) {
            logger.error("failed to list followings: {}", e.getMessage());
            return new FuncResponse<>("list following failed");
        }
    }

    @Override
    public FuncResponse<FollowingDeleteResponse> delete(FollowingDeleteRequest deleteRequest) {
        logger.info("start delete following w. data: {}", deleteRequest);
        try {
            var partitionKey = getPartitionKey(deleteRequest.getUsername());
            var sortKey = getSortKey(deleteRequest.getFollowingUsername());
            var followingKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var followingFound = dynamoFollowingEntityDynamoDbTable.getItem(followingKey);
            if (Objects.isNull(followingFound)) {
                throw new Exception("Following not found");
            }
            dynamoFollowingEntityDynamoDbTable.deleteItem(followingKey);
            logger.info("finish: delete following");
            return new FuncResponse<>(new FollowingDeleteResponse());
        } catch (Exception e) {
            logger.error("delete following failed: {}", e.getMessage());
            return new FuncResponse<>("delete following failed");
        }
    }
}
