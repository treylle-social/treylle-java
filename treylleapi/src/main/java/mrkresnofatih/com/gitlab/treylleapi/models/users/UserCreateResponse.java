package mrkresnofatih.com.gitlab.treylleapi.models.users;

public class UserCreateResponse {
    private String message;

    public UserCreateResponse() {
        this.message = "Create user Success";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
