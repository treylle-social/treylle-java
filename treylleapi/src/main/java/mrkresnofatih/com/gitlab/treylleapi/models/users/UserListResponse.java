package mrkresnofatih.com.gitlab.treylleapi.models.users;

import java.util.List;

public class UserListResponse {
    private List<UserGetResponse> users;

    public UserListResponse() {
    }

    public UserListResponse(List<UserGetResponse> users) {
        this.users = users;
    }

    public List<UserGetResponse> getUsers() {
        return users;
    }

    public void setUsers(List<UserGetResponse> users) {
        this.users = users;
    }
}
