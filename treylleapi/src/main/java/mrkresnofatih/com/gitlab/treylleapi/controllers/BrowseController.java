package mrkresnofatih.com.gitlab.treylleapi.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.browse.BrowseUserSearchRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.browse.BrowseUserSearchResponse;
import mrkresnofatih.com.gitlab.treylleapi.services.browse.BrowseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/browse")
public class BrowseController {
    private final BrowseService browseService;

    @Autowired
    public BrowseController(BrowseService browseService) {
        this.browseService = browseService;
    }

    @PostMapping("/search-users")
    public FuncResponse<BrowseUserSearchResponse> searchUsers(
            @Valid @RequestBody BrowseUserSearchRequest userSearchRequest) {
        return browseService.searchUser(userSearchRequest);
    }
}
