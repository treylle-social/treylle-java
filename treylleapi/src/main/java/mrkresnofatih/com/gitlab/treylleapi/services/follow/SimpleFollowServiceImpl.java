package mrkresnofatih.com.gitlab.treylleapi.services.follow;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.*;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.FollowerCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.FollowerDeleteRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.FollowerGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.FollowingCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.FollowingDeleteRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.FollowingGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.services.followers.FollowerService;
import mrkresnofatih.com.gitlab.treylleapi.services.followings.FollowingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimpleFollowServiceImpl implements FollowService {
    private final Logger logger;
    private final FollowerService followerService;
    private final FollowingService followingService;

    @Autowired
    public SimpleFollowServiceImpl(
            FollowerService followerService,
            FollowingService followingService) {
        this.followerService = followerService;
        this.followingService = followingService;
        this.logger = LoggerFactory.getLogger(SimpleFollowServiceImpl.class);
    }

    @Override
    public FuncResponse<FollowStartResponse> startFollow(FollowStartRequest startRequest) {
        logger.info("start startFollow w. data: {}", startRequest.toJsonSerialized());
        var followerGetResult = followerService
                .get(new FollowerGetRequest(startRequest.getUsernameToFollow(), startRequest.getUsername()));
        if (followerGetResult.isError()) {
            logger.debug("follower get result failed, will create follower data");
            var followerCreateResult = followerService
                    .create(new FollowerCreateRequest(startRequest.getUsernameToFollow(), startRequest.getUsername()));
            if (followerCreateResult.isError()) {
                logger.error("failed to create follower data");
                return new FuncResponse<>("failed to create follower data");
            }
        }
        var followingGetResult = followingService
                .get(new FollowingGetRequest(startRequest.getUsername(), startRequest.getUsernameToFollow()));
        if (followingGetResult.isError()) {
            logger.debug("following get result failed, will create following data");
            var followingCreateResult = followingService
                    .create(new FollowingCreateRequest(startRequest.getUsername(), startRequest.getUsernameToFollow()));
            if (followingCreateResult.isError()) {
                logger.error("failed to create following data");
                return new FuncResponse<>("failed to create following data");
            }
        }
        logger.info("finish startFollow");
        return new FuncResponse<>(new FollowStartResponse());
    }

    @Override
    public FuncResponse<FollowStopResponse> stopFollow(FollowStopRequest stopRequest) {
        logger.info("start stopFollow w. data: {}", stopRequest.toJsonSerialized());
        var followerGetResult = followerService
                .get(new FollowerGetRequest(stopRequest.getUsernameToUnfollow(), stopRequest.getUsername()));
        if (!followerGetResult.isError()) {
            logger.debug("follower data found, will attempt to delete");
            var followerDeleteResult = followerService
                    .delete(new FollowerDeleteRequest(stopRequest.getUsernameToUnfollow(), stopRequest.getUsername()));
            if (followerDeleteResult.isError()) {
                logger.error("failed to delete follower data");
                return new FuncResponse<>("failed to delete follower data");
            }
        }
        var followingGetResult = followingService
                .get(new FollowingGetRequest(stopRequest.getUsername(), stopRequest.getUsernameToUnfollow()));
        if (!followingGetResult.isError()) {
            logger.debug("following data found, will attempt to delete");
            var followingDeleteResult = followingService
                    .delete(new FollowingDeleteRequest(stopRequest.getUsername(), stopRequest.getUsernameToUnfollow()));
            if (followingDeleteResult.isError()) {
                logger.error("failed to delete following data");
                return new FuncResponse<>("failed to delete following data");
            }
        }
        logger.info("finished stopFollow");
        return new FuncResponse<>(new FollowStopResponse());
    }

    @Override
    public FuncResponse<FollowCheckIsFollowerResponse> checkIsFollower(FollowCheckIsFollowerRequest checkIsFollowerRequest) {
        logger.info("start checkIsFollower w. data: {}", checkIsFollowerRequest.toJsonSerialized());
        var followerGetResult = followerService
                .get(new FollowerGetRequest(checkIsFollowerRequest.getUsername(), checkIsFollowerRequest.getUsernameCandidateFollower()));
        if (followerGetResult.isError()) {
            return new FuncResponse<>(new FollowCheckIsFollowerResponse(false));
        }
        return new FuncResponse<>(new FollowCheckIsFollowerResponse(true));
    }

    @Override
    public FuncResponse<FollowCheckIsFollowingResponse> checkIsFollowing(FollowCheckIsFollowingRequest checkIsFollowingRequest) {
        logger.info("start checkIsFollowing w. data: {}", checkIsFollowingRequest.toJsonSerialized());
        var followerGetResult = followerService
                .get(new FollowerGetRequest(checkIsFollowingRequest.getUsernameCandidateFollowing(), checkIsFollowingRequest.getUsername()));
        if (followerGetResult.isError()) {
            return new FuncResponse<>(new FollowCheckIsFollowingResponse(false));
        }
        return new FuncResponse<>(new FollowCheckIsFollowingResponse(true));
    }
}
