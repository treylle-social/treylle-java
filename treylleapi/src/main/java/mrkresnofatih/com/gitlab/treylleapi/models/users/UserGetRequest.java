package mrkresnofatih.com.gitlab.treylleapi.models.users;

import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class UserGetRequest extends JsonSerializable {
    private String username;

    public UserGetRequest() {
    }

    public UserGetRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
