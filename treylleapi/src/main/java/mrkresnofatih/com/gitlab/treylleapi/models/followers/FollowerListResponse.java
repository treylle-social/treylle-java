package mrkresnofatih.com.gitlab.treylleapi.models.followers;

import java.util.List;

public class FollowerListResponse {
    private List<FollowerGetResponse> followers;

    public FollowerListResponse() {
    }

    public FollowerListResponse(List<FollowerGetResponse> followers) {
        this.followers = followers;
    }

    public List<FollowerGetResponse> getFollowers() {
        return followers;
    }

    public void setFollowers(List<FollowerGetResponse> followers) {
        this.followers = followers;
    }
}
