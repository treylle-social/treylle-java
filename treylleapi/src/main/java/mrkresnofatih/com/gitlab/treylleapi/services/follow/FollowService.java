package mrkresnofatih.com.gitlab.treylleapi.services.follow;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.*;

public interface FollowService {
    FuncResponse<FollowStartResponse> startFollow(FollowStartRequest startRequest);
    FuncResponse<FollowStopResponse> stopFollow(FollowStopRequest stopRequest);
    FuncResponse<FollowCheckIsFollowerResponse> checkIsFollower(FollowCheckIsFollowerRequest checkIsFollowerRequest);
    FuncResponse<FollowCheckIsFollowingResponse> checkIsFollowing(FollowCheckIsFollowingRequest checkIsFollowingRequest);
}
