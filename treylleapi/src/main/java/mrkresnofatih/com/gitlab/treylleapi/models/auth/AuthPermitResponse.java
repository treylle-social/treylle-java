package mrkresnofatih.com.gitlab.treylleapi.models.auth;

public class AuthPermitResponse {
    private boolean permitted;
    private String username;
    private String tokenId;

    public AuthPermitResponse() {
    }

    public AuthPermitResponse(boolean permitted, String username, String tokenId) {
        this.permitted = permitted;
        this.username = username;
        this.tokenId = tokenId;
    }

    public boolean isPermitted() {
        return permitted;
    }

    public void setPermitted(boolean permitted) {
        this.permitted = permitted;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
}
