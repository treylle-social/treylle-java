package mrkresnofatih.com.gitlab.treylleapi.models.followers;

public class FollowerGetResponse {
    private String username;
    private String followerUsername;

    public FollowerGetResponse() {
    }

    public FollowerGetResponse(String username, String followerUsername) {
        this.username = username;
        this.followerUsername = followerUsername;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFollowerUsername() {
        return followerUsername;
    }

    public void setFollowerUsername(String followerUsername) {
        this.followerUsername = followerUsername;
    }
}
