package mrkresnofatih.com.gitlab.treylleapi.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.*;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketPublishRequest;
import mrkresnofatih.com.gitlab.treylleapi.services.follow.FollowService;
import mrkresnofatih.com.gitlab.treylleapi.services.ticket.TicketService;
import mrkresnofatih.com.gitlab.treylleapi.utlities.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/follow")
public class FollowController {
    private final FollowService followService;
    private final TicketService ticketService;
    private final Logger logger;

    @Autowired
    public FollowController(FollowService followService, TicketService ticketService) {
        this.followService = followService;
        this.ticketService = ticketService;
        this.logger = LoggerFactory.getLogger(FollowController.class);
    }

    @PostMapping("/is-my-follower")
    public FuncResponse<FollowCheckIsFollowerResponse> isMyFollower(
            @RequestHeader("Claims-username") String username,
            @Valid @RequestBody FollowCheckIsFollowerRequest checkIsFollowerRequest) {
        checkIsFollowerRequest.setUsername(username);
        return followService.checkIsFollower(checkIsFollowerRequest);
    }

    @PostMapping("/is-my-following")
    public FuncResponse<FollowCheckIsFollowingResponse> isMyFollowing(
            @RequestHeader("Claims-username") String username,
            @Valid @RequestBody FollowCheckIsFollowingRequest checkIsFollowingRequest) {
        checkIsFollowingRequest.setUsername(username);
        return followService.checkIsFollowing(checkIsFollowingRequest);
    }

    @PostMapping("/start-follow")
    public FuncResponse<FollowStartResponse> startFollow(
            @RequestHeader("Claims-username") String username,
            @Valid @RequestBody FollowStartRequest startRequest) {
        startRequest.setUsername(username);
        var ticketSendResult = ticketService
                .publishTicket(new TicketPublishRequest(Constants.Ticketing.FollowTicketEvents, startRequest.toJsonSerialized()));
        if (ticketSendResult.isError()) {
            logger.warn(ticketSendResult.getErrorMessage());
        }
        return new FuncResponse<>(new FollowStartResponse());
    }

    @PostMapping("/stop-follow")
    public FuncResponse<FollowStopResponse> stopFollow(
            @RequestHeader("Claims-username") String username,
            @Valid @RequestBody FollowStopRequest stopRequest) {
        stopRequest.setUsername(username);
        var ticketSendResult = ticketService
                .publishTicket(new TicketPublishRequest(Constants.Ticketing.UnfollowTicketEvents, stopRequest.toJsonSerialized()));
        if (ticketSendResult.isError()) {
            logger.warn(ticketSendResult.getErrorMessage());
        }
        return new FuncResponse<>(new FollowStopResponse());
    }
}
