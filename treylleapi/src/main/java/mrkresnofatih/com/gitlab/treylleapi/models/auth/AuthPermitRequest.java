package mrkresnofatih.com.gitlab.treylleapi.models.auth;

import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class AuthPermitRequest extends JsonSerializable {
    private String token;

    public AuthPermitRequest() {
    }

    public AuthPermitRequest(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
