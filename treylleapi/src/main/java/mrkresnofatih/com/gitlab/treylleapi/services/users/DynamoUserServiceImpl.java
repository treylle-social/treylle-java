package mrkresnofatih.com.gitlab.treylleapi.services.users;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.users.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;

import java.util.ArrayList;
import java.util.Objects;

@Service
public class DynamoUserServiceImpl implements UserService {

    private final Logger logger;
    private final DynamoDbTable<DynamoUserEntityImpl> dynamoUserEntityDynamoDbTable;

    @Autowired
    public DynamoUserServiceImpl(DynamoDbTable<DynamoUserEntityImpl> dynamoUserEntityDynamoDbTable) {
        this.dynamoUserEntityDynamoDbTable = dynamoUserEntityDynamoDbTable;
        this.logger = LoggerFactory.getLogger(DynamoUserServiceImpl.class);
    }

    private String getPartitionKey() {
        return "Users";
    }

    private String getSortKey(String username) {
        return String.format("User_%s", username);
    }

    @Override
    public FuncResponse<UserCreateResponse> create(UserCreateRequest createRequest) {
        logger.info("Start create user w. data: {}", createRequest.toJsonSerialized());
        try {
            var newUser = new DynamoUserEntityImpl();
            newUser.setPartitionKey(getPartitionKey());
            newUser.setSortKey(getSortKey(createRequest.getUsername()));
            newUser.setUsername(createRequest.getUsername());
            newUser.setFullName(createRequest.getFullName());
            newUser.setAvatarUrl(createRequest.getAvatarUrl());
            newUser.setBio(createRequest.getBio());
            newUser.setJoinedAt(createRequest.getJoinedAt());
            newUser.setWallpaperUrl(createRequest.getWallpaperUrl());
            newUser.setPassword(createRequest.getPassword());

            dynamoUserEntityDynamoDbTable.putItem(newUser);
            var response = new UserCreateResponse();
            return new FuncResponse<>(response);
        } catch (Exception e) {
            logger.error("create user failed, error {}", e.getMessage());
            return new FuncResponse<>("failed to create user");
        }
    }

    @Override
    public FuncResponse<UserGetResponse> get(UserGetRequest getRequest) {
        logger.info("start get user w. data: {}", getRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey();
            var sortKey = getSortKey(getRequest.getUsername());
            var userKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var userFound = dynamoUserEntityDynamoDbTable.getItem(userKey);
            if (Objects.isNull(userFound)) {
                throw new Exception("User Not Found");
            }
            var userGetResponse = new UserGetResponse();
            userGetResponse.setUsername(userFound.getUsername());
            userGetResponse.setFullName(userFound.getFullName());
            userGetResponse.setAvatarUrl(userFound.getAvatarUrl());
            userGetResponse.setBio(userFound.getBio());
            userGetResponse.setJoinedAt(userFound.getJoinedAt());
            userGetResponse.setWallpaperUrl(userFound.getWallpaperUrl());
            userGetResponse.setPassword(userFound.getPassword());
            logger.info("finish get user");
            return new FuncResponse<>(userGetResponse);
        } catch (Exception e) {
            logger.error("Dynamodb get user failed: {}", e.getMessage());
            return new FuncResponse<>("dynamodb get user failed");
        }
    }

    @Override
    public FuncResponse<UserListResponse> list(UserListRequest listRequest) {
        logger.info("Start list users w. data: {}", listRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey();
            var sortKey = getSortKey(listRequest.getLastResult() == null ? "" : listRequest.getLastResult());
            if (!listRequest.getSearch().isEmpty()) {
                sortKey = getSortKey(listRequest.getSearch());
            }
            var startKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var queryConditional = !listRequest.getSearch().isEmpty() ?
                    QueryConditional.sortBeginsWith(startKey) : QueryConditional.sortGreaterThan(startKey);
            var query = QueryEnhancedRequest.builder()
                    .limit(listRequest.getPageSize())
                    .queryConditional(queryConditional)
                    .build();
            var result = dynamoUserEntityDynamoDbTable
                    .query(query)
                    .items()
                    .iterator();
            var userList = new ArrayList<UserGetResponse>();
            UserGetResponse userDetail;
            while (result.hasNext()) {
                userDetail = new UserGetResponse();
                var record = result.next();
                userDetail.setUsername(record.getUsername());
                userDetail.setFullName(record.getFullName());
                userDetail.setBio(record.getBio());
                userDetail.setWallpaperUrl(record.getWallpaperUrl());
                userDetail.setJoinedAt(record.getJoinedAt());
                userDetail.setAvatarUrl(record.getAvatarUrl());
                userList.add(userDetail);
            }
            var response = new UserListResponse(userList);
            logger.info("Finish list users");
            return new FuncResponse<>(response);
        } catch (Exception e) {
            logger.error("Dynamodb list user failed: {}", e.getMessage());
            return new FuncResponse<>("dynamodb list user failed");
        }
    }

    @Override
    public FuncResponse<UserUpdateResponse> update(UserUpdateRequest updateRequest) {
        logger.info("start update user w. data: {}", updateRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey();
            var sortKey = getSortKey(updateRequest.getUsername());
            var userKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var userFound = dynamoUserEntityDynamoDbTable.getItem(userKey);
            if (Objects.isNull(userFound)) {
                throw new Exception("User Not Found");
            }
            userFound.setFullName(updateRequest.getFullName());
            userFound.setBio(updateRequest.getBio());
            userFound.setWallpaperUrl(updateRequest.getWallpaperUrl());
            userFound.setAvatarUrl(updateRequest.getAvatarUrl());
            userFound.setJoinedAt(updateRequest.getJoinedAt());
            userFound.setPassword(updateRequest.getPassword());
            dynamoUserEntityDynamoDbTable.updateItem(userFound);
            var response = new UserUpdateResponse();
            logger.info("finish update user");
            return new FuncResponse<>(response);
        } catch (Exception e) {
            logger.error("Dynamodb update user failed: {}", e.getMessage());
            return new FuncResponse<>("dynamodb update user failed");
        }
    }

    @Override
    public FuncResponse<UserDeleteResponse> delete(UserDeleteRequest deleteRequest) {
        logger.info("start delete user w. data: {}", deleteRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey();
            var sortKey = getSortKey(deleteRequest.getUsername());
            var userKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var userFound = dynamoUserEntityDynamoDbTable.getItem(userKey);
            if (Objects.isNull(userFound)) {
                throw new Exception("User Not Found");
            }
            dynamoUserEntityDynamoDbTable.deleteItem(userKey);
            logger.error("success: delete user");
            return new FuncResponse<>(new UserDeleteResponse());
        } catch (Exception e) {
            logger.error("Dynamodb delete user failed: {}", e.getMessage());
            return new FuncResponse<>("dynamodb delete user failed");
        }
    }
}
