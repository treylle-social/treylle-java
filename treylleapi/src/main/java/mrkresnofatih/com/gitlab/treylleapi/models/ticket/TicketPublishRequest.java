package mrkresnofatih.com.gitlab.treylleapi.models.ticket;

import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

import java.util.List;
import java.util.UUID;

public class TicketPublishRequest extends JsonSerializable {
    private List<String> events;
    private String serializedData;
    private String ticketId;

    public TicketPublishRequest() {
    }

    public TicketPublishRequest(List<String> events, String serializedData) {
        this.events = events;
        this.serializedData = serializedData;
        this.ticketId = UUID.randomUUID().toString();
    }

    public TicketPublishRequest(List<String> events, String serializedData, String ticketId) {
        this.events = events;
        this.serializedData = serializedData;
        this.ticketId = ticketId;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }

    public String getSerializedData() {
        return serializedData;
    }

    public void setSerializedData(String serializedData) {
        this.serializedData = serializedData;
    }
}
