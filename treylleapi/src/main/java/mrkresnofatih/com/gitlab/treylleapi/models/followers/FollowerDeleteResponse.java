package mrkresnofatih.com.gitlab.treylleapi.models.followers;

public class FollowerDeleteResponse {
    private String message;

    public FollowerDeleteResponse() {
        this.message = "Delete follower success";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
