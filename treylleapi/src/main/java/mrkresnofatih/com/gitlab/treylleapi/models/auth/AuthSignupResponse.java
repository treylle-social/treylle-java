package mrkresnofatih.com.gitlab.treylleapi.models.auth;

public class AuthSignupResponse {
    private String message;

    public AuthSignupResponse() {
        this.message = "signup success";
    }

    public AuthSignupResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
