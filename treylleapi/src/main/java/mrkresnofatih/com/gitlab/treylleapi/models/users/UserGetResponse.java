package mrkresnofatih.com.gitlab.treylleapi.models.users;

public class UserGetResponse {
    private String username;
    private String fullName;
    private String avatarUrl;
    private String bio;
    private String wallpaperUrl;
    private String joinedAt;
    private String password;

    public UserGetResponse() {
    }

    public UserGetResponse(
            String username,
            String fullName,
            String avatarUrl,
            String bio,
            String wallpaperUrl,
            String joinedAt,
            String password) {
        this.username = username;
        this.fullName = fullName;
        this.avatarUrl = avatarUrl;
        this.bio = bio;
        this.wallpaperUrl = wallpaperUrl;
        this.joinedAt = joinedAt;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getWallpaperUrl() {
        return wallpaperUrl;
    }

    public void setWallpaperUrl(String wallpaperUrl) {
        this.wallpaperUrl = wallpaperUrl;
    }

    public String getJoinedAt() {
        return joinedAt;
    }

    public void setJoinedAt(String joinedAt) {
        this.joinedAt = joinedAt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
