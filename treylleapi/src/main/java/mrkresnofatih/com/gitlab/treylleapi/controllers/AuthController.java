package mrkresnofatih.com.gitlab.treylleapi.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.*;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketPublishRequest;
import mrkresnofatih.com.gitlab.treylleapi.services.auth.AuthService;
import mrkresnofatih.com.gitlab.treylleapi.services.ticket.TicketService;
import mrkresnofatih.com.gitlab.treylleapi.utlities.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    private final AuthService authService;
    private final TicketService ticketService;
    private final Logger logger;

    @Autowired
    public AuthController(AuthService authService, TicketService ticketService) {
        this.authService = authService;
        this.ticketService = ticketService;
        this.logger = LoggerFactory.getLogger(AuthController.class);
    }

    @PostMapping("/signup")
    public FuncResponse<AuthSignupResponse> signup(@Valid @RequestBody AuthSignupRequest signupRequest) {
        var response = ticketService
                .publishTicket(new TicketPublishRequest(Constants.Ticketing.SignupTicketEvents, signupRequest.toJsonSerialized()));
        if (response.isError()) {
            logger.warn(response.getErrorMessage());
        }
        return new FuncResponse<>(new AuthSignupResponse());
    }

    @PostMapping("/login")
    public FuncResponse<AuthLoginResponse> login(@Valid @RequestBody AuthLoginRequest loginRequest) {
        return authService.login(loginRequest);
    }

    @PostMapping("/checkSignup")
    public FuncResponse<AuthSignupCheckResponse> checkSignup(@Valid @RequestBody AuthSignupCheckRequest checkRequest) {
        return authService.checkSignup(checkRequest);
    }
}
