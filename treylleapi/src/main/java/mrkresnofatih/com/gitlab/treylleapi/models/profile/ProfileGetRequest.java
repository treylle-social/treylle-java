package mrkresnofatih.com.gitlab.treylleapi.models.profile;

import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class ProfileGetRequest extends JsonSerializable {
    private String username;

    public ProfileGetRequest() {
    }

    public ProfileGetRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
