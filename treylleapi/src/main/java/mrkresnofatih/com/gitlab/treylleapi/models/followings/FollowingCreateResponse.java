package mrkresnofatih.com.gitlab.treylleapi.models.followings;

public class FollowingCreateResponse {
    private String message;

    public FollowingCreateResponse() {
        this.message = "Following created successfully";
    }

    public FollowingCreateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
