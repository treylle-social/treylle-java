package mrkresnofatih.com.gitlab.treylleapi.models.jwt;

import java.util.Map;

public class JwtValidateResponse {
    private boolean valid;
    private Map<String, String> extractedClaims;

    public JwtValidateResponse() {
    }

    public JwtValidateResponse(boolean valid, Map<String, String> extractedClaims) {
        this.valid = valid;
        this.extractedClaims = extractedClaims;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Map<String, String> getExtractedClaims() {
        return extractedClaims;
    }

    public void setExtractedClaims(Map<String, String> extractedClaims) {
        this.extractedClaims = extractedClaims;
    }
}
