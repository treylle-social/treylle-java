package mrkresnofatih.com.gitlab.treylleapi.configurations;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig {
    @Value("${treylle.env.selected}")
    private String environment;

    @Value("${treylle.aws.profile}")
    private String profile;

    @Value("${treylle.aws.region}")
    private String region;

    AWSCredentialsProvider getAwsCredentialsProvider() {
        if (environment.equals("local")) {
            return new ProfileCredentialsProvider(profile);
        }
        return new EnvironmentVariableCredentialsProvider();
    }

    @Bean
    public QueueMessagingTemplate queueMessagingTemplate() {
        return new QueueMessagingTemplate(amazonSQSAsync());
    }

    @Bean
    public AmazonSQSAsync amazonSQSAsync() {
        return AmazonSQSAsyncClientBuilder
                .standard()
                .withRegion(region)
                .withCredentials(getAwsCredentialsProvider())
                .build();
    }
}
