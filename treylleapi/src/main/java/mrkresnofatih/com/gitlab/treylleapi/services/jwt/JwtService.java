package mrkresnofatih.com.gitlab.treylleapi.services.jwt;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtCreateResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtValidateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtValidateResponse;

public interface JwtService {
    FuncResponse<JwtCreateResponse> create(JwtCreateRequest createRequest);
    FuncResponse<JwtValidateResponse> validate(JwtValidateRequest validateRequest);
}
