package mrkresnofatih.com.gitlab.treylleapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TreylleapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreylleapiApplication.class, args);
	}

}
