package mrkresnofatih.com.gitlab.treylleapi.configurations;

public class TreylleAwsResourceConfig {
    private String dynamoDbTableName;
    private String sqsQueueName;

    public TreylleAwsResourceConfig() {
    }

    public TreylleAwsResourceConfig(String dynamoDbTableName, String sqsQueueName) {
        this.dynamoDbTableName = dynamoDbTableName;
        this.sqsQueueName = sqsQueueName;
    }

    public String getDynamoDbTableName() {
        return dynamoDbTableName;
    }

    public void setDynamoDbTableName(String dynamoDbTableName) {
        this.dynamoDbTableName = dynamoDbTableName;
    }

    public String getSqsQueueName() {
        return sqsQueueName;
    }

    public void setSqsQueueName(String sqsQueueName) {
        this.sqsQueueName = sqsQueueName;
    }
}
