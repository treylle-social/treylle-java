package mrkresnofatih.com.gitlab.treylleapi.utlities;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;

public class JsonUtility {
    public static FuncResponse<String> serialize(Object object) {
        try {
            var mapper = new ObjectMapper();
            return new FuncResponse<>(mapper.writeValueAsString(object), "");
        } catch (JsonProcessingException exception) {
            return new FuncResponse<>(null, "failed to serialize");
        }
    }

    public static <T> FuncResponse<T> deserialize(String data, Class<T> type) {
        try {
            var mapper = new ObjectMapper();
            return new FuncResponse<>(mapper.readValue(data, type), "");
        } catch (JsonProcessingException exception) {
            return new FuncResponse<>("failed to serialize");
        }
    }
}
