package mrkresnofatih.com.gitlab.treylleapi.configurations;

public class TreylleConfig {
    private String authSecret;
    private String authIssuer;

    public TreylleConfig() {
    }

    public TreylleConfig(String authSecret, String authIssuer) {
        this.authSecret = authSecret;
        this.authIssuer = authIssuer;
    }

    public String getAuthSecret() {
        return authSecret;
    }

    public void setAuthSecret(String authSecret) {
        this.authSecret = authSecret;
    }

    public String getAuthIssuer() {
        return authIssuer;
    }

    public void setAuthIssuer(String authIssuer) {
        this.authIssuer = authIssuer;
    }
}
