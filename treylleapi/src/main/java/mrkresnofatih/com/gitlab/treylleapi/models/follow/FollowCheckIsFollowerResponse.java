package mrkresnofatih.com.gitlab.treylleapi.models.follow;

public class FollowCheckIsFollowerResponse {
    private boolean isFollower;

    public FollowCheckIsFollowerResponse() {
    }

    public FollowCheckIsFollowerResponse(boolean isFollower) {
        this.isFollower = isFollower;
    }

    public boolean isFollower() {
        return isFollower;
    }

    public void setFollower(boolean follower) {
        isFollower = follower;
    }
}
