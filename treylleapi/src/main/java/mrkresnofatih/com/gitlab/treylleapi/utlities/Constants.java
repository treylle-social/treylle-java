package mrkresnofatih.com.gitlab.treylleapi.utlities;

import java.util.List;

public class Constants {
    public static class Ticketing {
        public static final String AUTH_SIGNUP = "AUTH.SIGNUP";
        public static final List<String> SignupTicketEvents = List.of(AUTH_SIGNUP);

        public static final String FOLLOW_START_FOLLOW = "FOLLOW.START_FOLLOW";
        public static final List<String> FollowTicketEvents = List.of(FOLLOW_START_FOLLOW);

        public static final String FOLLOW_STOP_FOLLOW = "FOLLOW.STOP_FOLLOW";
        public static final List<String> UnfollowTicketEvents = List.of(FOLLOW_STOP_FOLLOW);

    }
}
