package mrkresnofatih.com.gitlab.treylleapi.services.hash;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;

public interface HashService {
    FuncResponse<String> hash(String data);
    FuncResponse<Boolean> compare(String plainData, String hashedData);
}
