package mrkresnofatih.com.gitlab.treylleapi.models.followings;

import java.util.List;

public class FollowingListResponse {
    private List<FollowingGetResponse> followings;

    public FollowingListResponse() {
    }

    public FollowingListResponse(List<FollowingGetResponse> followings) {
        this.followings = followings;
    }

    public List<FollowingGetResponse> getFollowings() {
        return followings;
    }

    public void setFollowings(List<FollowingGetResponse> followings) {
        this.followings = followings;
    }
}
