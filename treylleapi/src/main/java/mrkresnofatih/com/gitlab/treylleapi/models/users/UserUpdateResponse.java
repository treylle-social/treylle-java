package mrkresnofatih.com.gitlab.treylleapi.models.users;

public class UserUpdateResponse {
    private String message;

    public UserUpdateResponse() {
        this.message = "Update user Success";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
