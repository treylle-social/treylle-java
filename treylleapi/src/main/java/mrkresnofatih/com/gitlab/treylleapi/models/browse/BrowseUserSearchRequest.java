package mrkresnofatih.com.gitlab.treylleapi.models.browse;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;
import org.hibernate.validator.constraints.Range;

public class BrowseUserSearchRequest extends JsonSerializable {
    @NotNull
    private String query;

    @NotNull
    private String lastResult;

    @NotNull
    @Range(min = 5, max = 100)
    private int pageSize;

    public BrowseUserSearchRequest() {
    }

    public BrowseUserSearchRequest(String query, String lastResult, int pageSize) {
        this.query = query;
        this.lastResult = lastResult;
        this.pageSize = pageSize;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getLastResult() {
        return lastResult;
    }

    public void setLastResult(String lastResult) {
        this.lastResult = lastResult;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
