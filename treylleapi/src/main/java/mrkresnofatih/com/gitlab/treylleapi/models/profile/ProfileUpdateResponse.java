package mrkresnofatih.com.gitlab.treylleapi.models.profile;

public class ProfileUpdateResponse {
    private String message;

    public ProfileUpdateResponse() {
        this.message = "Profile Updated Successfully";
    }

    public ProfileUpdateResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
