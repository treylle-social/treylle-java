package mrkresnofatih.com.gitlab.treylleapi.services.auth;

import mrkresnofatih.com.gitlab.treylleapi.services.hash.HashService;
import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.*;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtValidateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserListRequest;
import mrkresnofatih.com.gitlab.treylleapi.services.jwt.JwtService;
import mrkresnofatih.com.gitlab.treylleapi.services.users.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

@Service
public class SimpleAuthService implements AuthService {
    private final JwtService jwtService;
    private final HashService hashService;
    private final UserService userService;
    private final Logger logger;
    private final String USERNAME_CLAIM_KEY = "X-Username";
    private final String USER_TOKEN_ID_CLAIM_KEY = "X-User-Token-Id";
    private final String DEFAULT_AVATAR_URL = "https://images.unsplash.com/photo-1490650034439-fd184c3c86a5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MXwxNDk0OTAwfHxlbnwwfHx8fHw%3D&w=1000&q=80";
    private final String DEFAULT_WALLPAPER_URL = "https://www.hdwallpapers.in/thumbs/2022/dune_sandwich_harbour_namibia_hd_travel-t2.jpg";

    public SimpleAuthService(JwtService jwtService, HashService hashService, UserService userService) {
        this.jwtService = jwtService;
        this.hashService = hashService;
        this.userService = userService;
        this.logger = LoggerFactory.getLogger(SimpleAuthService.class);
    }

    @Override
    public FuncResponse<AuthLoginResponse> login(AuthLoginRequest loginRequest) {
        logger.info("start login w. data: {}", loginRequest.getUsername());
        var userGetResult = userService.get(new UserGetRequest(loginRequest.getUsername()));
        if (userGetResult.isError()) {
            logger.info("user get failed");
            return new FuncResponse<>("user get w. username failed");
        }
        var userData = userGetResult.getData();
        var passwordValidityResult = hashService.compare(loginRequest.getPassword(), userData.getPassword());
        if (passwordValidityResult.isError()) {
            logger.error("failed to validate password");
            return new FuncResponse<>("failed to validate password");
        }
        var validity = passwordValidityResult.getData();
        if (!validity) {
            logger.error("invalid credentials");
            return new FuncResponse<>("invalid credentials");
        }
        var claims = new HashMap<String, String>();
        claims.put(USERNAME_CLAIM_KEY, userData.getUsername());
        claims.put(USER_TOKEN_ID_CLAIM_KEY, UUID.randomUUID().toString());
        var jwtCreateRequest = new JwtCreateRequest();
        jwtCreateRequest.setClaims(claims);
        jwtCreateRequest.setLifeTime(86400L);
        var tokenResult = jwtService.create(jwtCreateRequest);
        if (tokenResult.isError()) {
            logger.error("failed to create user jwt token");
            return new FuncResponse<>("failed to create user jwt token");
        }
        var token = tokenResult.getData().getToken();
        logger.info("finish login");
        return new FuncResponse<>(new AuthLoginResponse(token));
    }

    @Override
    public FuncResponse<AuthSignupCheckResponse> checkSignup(AuthSignupCheckRequest signupCheckRequest) {
        logger.info("start checkSignup w. data: {}", signupCheckRequest.toJsonSerialized());
        var userListResult = userService
                .list(new UserListRequest(signupCheckRequest.getUsername(), null, 25));
        if (userListResult.isError()) {
            logger.error("user list error");
            return new FuncResponse<>("user list failed");
        }
        var userFound = userListResult.getData().getUsers()
                .stream()
                .anyMatch(user -> user.getUsername().equals(signupCheckRequest.getUsername()));
        if (userFound) {
            logger.info("user with username {} already exists", signupCheckRequest.getUsername());
            var response = new AuthSignupCheckResponse();
            response.setUserNameTaken(true);
            return new FuncResponse<>(response);
        }
        logger.info("finish checkSignup");
        return new FuncResponse<>(new AuthSignupCheckResponse(false));
    }

    @Override
    public FuncResponse<AuthSignupResponse> signup(AuthSignupRequest signupRequest) {
        logger.info("start signup w. data: {}", signupRequest.getUsername());
        var checkSignupResult = checkSignup(new AuthSignupCheckRequest(signupRequest.getUsername()));
        if (checkSignupResult.isError()) {
            logger.error("user check signup failed");
            return new FuncResponse<>("user check signup failed");
        }
        if (checkSignupResult.getData().isUserNameTaken()) {
            logger.error("username taken");
            return new FuncResponse<>("username taken");
        }
        var userCreateRequest = new UserCreateRequest();
        userCreateRequest.setUsername(signupRequest.getUsername());
        userCreateRequest.setFullName(signupRequest.getFullName());
        userCreateRequest.setAvatarUrl(DEFAULT_AVATAR_URL);
        userCreateRequest.setWallpaperUrl(DEFAULT_WALLPAPER_URL);
        userCreateRequest.setJoinedAt(_getCurrentJoinedAt());
        userCreateRequest.setBio(String.format("I am %s", signupRequest.getFullName()));
        var hashedPasswordResult = hashService.hash(signupRequest.getPassword());
        if (hashedPasswordResult.isError()) {
            logger.error("failed to hash password");
            return new FuncResponse<>("failed to hash password");
        }
        var hashedPassword = hashedPasswordResult.getData();
        userCreateRequest.setPassword(hashedPassword);
        var userCreateResult = userService.create(userCreateRequest);
        if (userCreateResult.isError()) {
            logger.error("failed to create user");
            return new FuncResponse<>("failed to create user");
        }
        logger.info("finished signup");
        return new FuncResponse<>(new AuthSignupResponse());
    }

    @Override
    public FuncResponse<AuthPermitResponse> permit(AuthPermitRequest permitRequest) {
        logger.info("start permit w. data: {}", permitRequest.toJsonSerialized());
        var claimKeys = new HashSet<String>();
        claimKeys.add(USERNAME_CLAIM_KEY);
        claimKeys.add(USER_TOKEN_ID_CLAIM_KEY);
        var validationRequest = new JwtValidateRequest();
        validationRequest.setToken(permitRequest.getToken());
        validationRequest.setExtractionClaims(claimKeys);
        var validationResult = jwtService.validate(validationRequest);
        if (validationResult.isError()) {
            logger.error("jwt token validation failed or invalid");
            return new FuncResponse<>("jwt token validation failed or token invalid");
        }
        var isValid = validationResult.getData().isValid();
        if (!isValid) {
            logger.error("token validation failed");
            return new FuncResponse<>("token is invalid");
        }
        var extractedClaims = validationResult.getData().getExtractedClaims();
        var response = new AuthPermitResponse();
        response.setUsername(extractedClaims.get(USERNAME_CLAIM_KEY));
        response.setTokenId(extractedClaims.get(USER_TOKEN_ID_CLAIM_KEY));
        response.setPermitted(true);
        logger.info("finish auth permit");
        return new FuncResponse<>(response);
    }

    private String _getCurrentJoinedAt() {
        var currentDate = LocalDate.now();
        var currentMonth = currentDate.getMonth().toString();
        var currentYear = String.valueOf(currentDate.getYear());
        return String.format("%s %s", currentMonth, currentYear);
    }
}
