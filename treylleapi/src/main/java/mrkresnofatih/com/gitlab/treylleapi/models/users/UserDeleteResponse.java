package mrkresnofatih.com.gitlab.treylleapi.models.users;

public class UserDeleteResponse {
    private String message;

    public UserDeleteResponse() {
        this.message = "Delete user success";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
