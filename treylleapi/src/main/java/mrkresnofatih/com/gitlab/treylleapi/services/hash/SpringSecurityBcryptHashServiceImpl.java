package mrkresnofatih.com.gitlab.treylleapi.services.hash;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class SpringSecurityBcryptHashServiceImpl implements HashService {
    private final Logger logger;

    @Autowired
    public SpringSecurityBcryptHashServiceImpl() {
        this.logger = LoggerFactory.getLogger(SpringSecurityBcryptHashServiceImpl.class);
    }

    @Override
    public FuncResponse<String> hash(String data) {
        logger.debug("start hash data: {}", data);
        var salt = BCrypt.gensalt();
        var hash = BCrypt.hashpw(data, salt);
        return new FuncResponse<>(hash, "");
    }

    @Override
    public FuncResponse<Boolean> compare(String plainData, String hashedData) {
        logger.debug("start compare hash: {} and {}: ", plainData, hashedData);
        return new FuncResponse<>(BCrypt.checkpw(plainData, hashedData));
    }
}
