package mrkresnofatih.com.gitlab.treylleapi.models.auth;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class AuthSignupRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;

    @NotNull
    @NotBlank
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[^A-Za-z0-9]).{8,}$")
    private String password;

    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9\\s]{1,50}$")
    private String fullName;

    public AuthSignupRequest() {
    }

    public AuthSignupRequest(String username, String password, String fullName) {
        this.username = username;
        this.password = password;
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
