package mrkresnofatih.com.gitlab.treylleapi.services.profile;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileGetResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileUpdateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileUpdateResponse;

public interface ProfileService {
    FuncResponse<ProfileGetResponse> get(ProfileGetRequest getRequest);
    FuncResponse<ProfileUpdateResponse> update(ProfileUpdateRequest updateRequest);
}
