package mrkresnofatih.com.gitlab.treylleapi.models.jwt;

public class JwtCreateResponse {
    private String token;

    public JwtCreateResponse() {
    }

    public JwtCreateResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
