package mrkresnofatih.com.gitlab.treylleapi.models.follow;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class FollowCheckIsFollowingRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;

    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String usernameCandidateFollowing;

    public FollowCheckIsFollowingRequest() {
    }

    public FollowCheckIsFollowingRequest(String username, String usernameCandidateFollowing) {
        this.username = username;
        this.usernameCandidateFollowing = usernameCandidateFollowing;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameCandidateFollowing() {
        return usernameCandidateFollowing;
    }

    public void setUsernameCandidateFollowing(String usernameCandidateFollowing) {
        this.usernameCandidateFollowing = usernameCandidateFollowing;
    }
}
