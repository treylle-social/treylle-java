package mrkresnofatih.com.gitlab.treylleapi.controllers;

import jakarta.validation.Valid;
import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileGetResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileUpdateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileUpdateResponse;
import mrkresnofatih.com.gitlab.treylleapi.services.profile.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/profile")
public class ProfileController {
    private final ProfileService profileService;

    @Autowired
    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @PostMapping("/get")
    public FuncResponse<ProfileGetResponse> get(
            @RequestParam(required = false) String username,
            @RequestHeader("Claims-username") String requesterUsername) {
        if (username != null) {
            return profileService.get(new ProfileGetRequest(username));
        }
        return profileService.get(new ProfileGetRequest(requesterUsername));
    }

    @PostMapping("/update")
    public FuncResponse<ProfileUpdateResponse> update(
            @Valid @RequestBody ProfileUpdateRequest updateRequest,
            @RequestHeader("Claims-username") String username) {
        updateRequest.setUsername(username);
        return profileService.update(updateRequest);
    }
}
