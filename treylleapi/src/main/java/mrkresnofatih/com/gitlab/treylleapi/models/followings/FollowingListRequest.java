package mrkresnofatih.com.gitlab.treylleapi.models.followings;

import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class FollowingListRequest extends JsonSerializable {
    private String username;
    private String lastResult;
    private int pageSize;

    public FollowingListRequest() {
    }

    public FollowingListRequest(String username, String lastResult, int pageSize) {
        this.username = username;
        this.lastResult = lastResult;
        this.pageSize = pageSize;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastResult() {
        return lastResult;
    }

    public void setLastResult(String lastResult) {
        this.lastResult = lastResult;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
