package mrkresnofatih.com.gitlab.treylleapi.models.follow;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class FollowStopRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;

    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String usernameToUnfollow;

    public FollowStopRequest() {
    }

    public FollowStopRequest(String username, String usernameToUnfollow) {
        this.username = username;
        this.usernameToUnfollow = usernameToUnfollow;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameToUnfollow() {
        return usernameToUnfollow;
    }

    public void setUsernameToUnfollow(String usernameToUnfollow) {
        this.usernameToUnfollow = usernameToUnfollow;
    }
}
