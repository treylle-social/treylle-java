package mrkresnofatih.com.gitlab.treylleapi.models.follow;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class FollowCheckIsFollowerRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;

    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String usernameCandidateFollower;

    public FollowCheckIsFollowerRequest() {
    }

    public FollowCheckIsFollowerRequest(String username, String usernameCandidateFollower) {
        this.username = username;
        this.usernameCandidateFollower = usernameCandidateFollower;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameCandidateFollower() {
        return usernameCandidateFollower;
    }

    public void setUsernameCandidateFollower(String usernameCandidateFollower) {
        this.usernameCandidateFollower = usernameCandidateFollower;
    }
}
