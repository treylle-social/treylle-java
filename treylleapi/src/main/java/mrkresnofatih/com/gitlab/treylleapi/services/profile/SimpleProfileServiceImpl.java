package mrkresnofatih.com.gitlab.treylleapi.services.profile;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileGetResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileUpdateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileUpdateResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserUpdateRequest;
import mrkresnofatih.com.gitlab.treylleapi.services.users.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimpleProfileServiceImpl implements ProfileService {
    private final Logger logger;
    private final UserService userService;

    @Autowired
    public SimpleProfileServiceImpl(UserService userService) {
        this.userService = userService;
        this.logger = LoggerFactory.getLogger(SimpleProfileServiceImpl.class);
    }

    @Override
    public FuncResponse<ProfileGetResponse> get(ProfileGetRequest getRequest) {
        logger.info("start get profile w. data: {}", getRequest.toJsonSerialized());
        var userGetResult = userService
                .get(new UserGetRequest(getRequest.getUsername()));
        if (userGetResult.isError()) {
            logger.error("user get failed");
            return new FuncResponse<>("failed to get user");
        }
        var userData = userGetResult.getData();
        var profileGetResponse = new ProfileGetResponse();
        profileGetResponse.setUsername(userData.getUsername());
        profileGetResponse.setAvatarUrl(userData.getAvatarUrl());
        profileGetResponse.setBio(userData.getBio());
        profileGetResponse.setAvatarUrl(userData.getAvatarUrl());
        profileGetResponse.setFullName(userData.getFullName());
        profileGetResponse.setJoinedAt(userData.getJoinedAt());
        profileGetResponse.setWallpaperUrl(userData.getWallpaperUrl());
        logger.info("get user profile success");
        return new FuncResponse<>(profileGetResponse);
    }

    @Override
    public FuncResponse<ProfileUpdateResponse> update(ProfileUpdateRequest updateRequest) {
        logger.info("start update profile w. data: {}", updateRequest.toJsonSerialized());
        var userGetResult = userService
                .get(new UserGetRequest(updateRequest.getUsername()));
        if (userGetResult.isError()) {
            logger.error("failed to get user");
            return new FuncResponse<>("failed to get user");
        }
        var userData = userGetResult.getData();
        var userUpdateReq = new UserUpdateRequest();
        userUpdateReq.setUsername(updateRequest.getUsername());
        userUpdateReq.setFullName(updateRequest.getFullName());
        userUpdateReq.setBio(updateRequest.getBio());
        userUpdateReq.setAvatarUrl(updateRequest.getAvatarUrl());
        userUpdateReq.setWallpaperUrl(updateRequest.getWallpaperUrl());
        userUpdateReq.setJoinedAt(userData.getJoinedAt());
        userUpdateReq.setPassword(userData.getPassword());
        var userUpdateResult = userService
                .update(userUpdateReq);
        if (userUpdateResult.isError()) {
            logger.error("failed to update user profile");
            return new FuncResponse<>("failed to update user profile");
        }
        logger.info("update user profile success");
        return new FuncResponse<>(new ProfileUpdateResponse());
    }
}
