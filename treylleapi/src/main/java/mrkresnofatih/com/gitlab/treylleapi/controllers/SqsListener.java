package mrkresnofatih.com.gitlab.treylleapi.controllers;

import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import mrkresnofatih.com.gitlab.treylleapi.configurations.TreylleAwsResourceConfig;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.AuthSignupRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.FollowStartRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.FollowStopRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketModel;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketPublishRequest;
import mrkresnofatih.com.gitlab.treylleapi.services.auth.AuthService;
import mrkresnofatih.com.gitlab.treylleapi.services.follow.FollowService;
import mrkresnofatih.com.gitlab.treylleapi.services.ticket.TicketService;
import mrkresnofatih.com.gitlab.treylleapi.utlities.Constants;
import mrkresnofatih.com.gitlab.treylleapi.utlities.JsonUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SqsListener {
    private final Logger logger;
    private final QueueMessagingTemplate queueMessagingTemplate;
    private final AuthService authService;
    private final FollowService followService;
    private final TreylleAwsResourceConfig treylleAwsResourceConfig;
    private final TicketService ticketService;

    @Autowired
    public SqsListener(QueueMessagingTemplate queueMessagingTemplate,
                       AuthService authService,
                       FollowService followService,
                       TreylleAwsResourceConfig treylleAwsResourceConfig,
                       TicketService ticketService) {
        this.queueMessagingTemplate = queueMessagingTemplate;
        this.authService = authService;
        this.followService = followService;
        this.treylleAwsResourceConfig = treylleAwsResourceConfig;
        this.ticketService = ticketService;
        this.logger = LoggerFactory.getLogger(SqsListener.class);
    }

    @Scheduled(fixedDelay = 1000)
    @Async
    public void handleQueue() {
        var ticketData = queueMessagingTemplate
                .receiveAndConvert(treylleAwsResourceConfig.getSqsQueueName(), String.class);
        if (ticketData == null) {
            return;
        }
        logger.info("start handleQueue w. data: {}", ticketData);
        var ticketParseResult = JsonUtility.deserialize(ticketData, TicketModel.class);
        if (ticketParseResult.isError()) {
            logger.error("failed to json-parse ticket data");
            return;
        }
        var ticket = ticketParseResult.getData();
        if (ticket.getEvents().isEmpty()) {
            return;
        }
        logger.info("processing ticket w. ticketId: {}", ticket.getTicketId());
        var events = ticket.getEvents();
        var eventName = events.get(0);
        logger.info("processing ticket for event: {}", eventName);
        switch (eventName) {
            case Constants.Ticketing.FOLLOW_STOP_FOLLOW:
                var stopFollowDataResult = JsonUtility.deserialize(ticket.getSerializedData(), FollowStopRequest.class);
                var stopFollowResult = followService.stopFollow(stopFollowDataResult.getData());
                if (stopFollowResult.isError()) {
                    logger.error("failed to stop follow");
                }
                break;
            case Constants.Ticketing.FOLLOW_START_FOLLOW:
                var startFollowDataResult = JsonUtility.deserialize(ticket.getSerializedData(), FollowStartRequest.class);
                var startFollowResult = followService.startFollow(startFollowDataResult.getData());
                if (startFollowResult.isError()) {
                    logger.error("failed to start follow");
                }
                break;
            case Constants.Ticketing.AUTH_SIGNUP:
                var signupDataResult = JsonUtility.deserialize(ticket.getSerializedData(), AuthSignupRequest.class);
                var signupResult = authService.signup(signupDataResult.getData());
                if (signupResult.isError()) {
                    logger.error("failed to signup");
                }
                break;
            default:
                break;
        }

        logger.debug("popping event from the ticket event-stack data");
        events.remove(0);
        if (events.size() <= 0) {
            logger.info("ticket event-stack has no more remaining events to process");
            return;
        }
        var submitTicketResult = ticketService
                .publishTicket(new TicketPublishRequest(events, ticket.getSerializedData(), ticket.getTicketId()));
        if (submitTicketResult.isError()) {
            logger.error("failed to continue ticket");
        }
    }
}
