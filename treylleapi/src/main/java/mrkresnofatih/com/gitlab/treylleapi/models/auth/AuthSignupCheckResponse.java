package mrkresnofatih.com.gitlab.treylleapi.models.auth;

public class AuthSignupCheckResponse {
    private boolean userNameTaken;

    public AuthSignupCheckResponse() {
    }

    public AuthSignupCheckResponse(boolean userNameTaken) {
        this.userNameTaken = userNameTaken;
    }

    public boolean isUserNameTaken() {
        return userNameTaken;
    }

    public void setUserNameTaken(boolean userNameTaken) {
        this.userNameTaken = userNameTaken;
    }
}
