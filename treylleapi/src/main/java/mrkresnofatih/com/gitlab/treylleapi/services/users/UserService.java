package mrkresnofatih.com.gitlab.treylleapi.services.users;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.users.*;

public interface UserService {
    FuncResponse<UserCreateResponse> create(UserCreateRequest createRequest);
    FuncResponse<UserGetResponse> get(UserGetRequest getRequest);
    FuncResponse<UserListResponse> list(UserListRequest listRequest);
    FuncResponse<UserUpdateResponse> update(UserUpdateRequest updateRequest);
    FuncResponse<UserDeleteResponse> delete(UserDeleteRequest deleteRequest);
}
