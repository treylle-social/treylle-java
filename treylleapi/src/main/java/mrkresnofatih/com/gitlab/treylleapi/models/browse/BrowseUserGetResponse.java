package mrkresnofatih.com.gitlab.treylleapi.models.browse;

public class BrowseUserGetResponse {
    private String username;
    private String fullName;
    private String avatarUrl;
    private String bio;

    public BrowseUserGetResponse() {
    }

    public BrowseUserGetResponse(String username, String fullName, String avatarUrl, String bio) {
        this.username = username;
        this.fullName = fullName;
        this.avatarUrl = avatarUrl;
        this.bio = bio;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
