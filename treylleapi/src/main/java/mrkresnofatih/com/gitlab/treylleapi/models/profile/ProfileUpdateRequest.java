package mrkresnofatih.com.gitlab.treylleapi.models.profile;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import mrkresnofatih.com.gitlab.treylleapi.models.JsonSerializable;

public class ProfileUpdateRequest extends JsonSerializable {
    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{6,30}$")
    private String username;

    @NotNull
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9\\s]{1,50}$")
    private String fullName;

    @NotNull
    @NotBlank
    private String avatarUrl;

    @NotNull
    @NotBlank
    private String bio;

    @NotNull
    @NotBlank
    private String wallpaperUrl;

    public ProfileUpdateRequest() {
    }

    public ProfileUpdateRequest(String username, String fullName, String avatarUrl, String bio, String wallpaperUrl) {
        this.username = username;
        this.fullName = fullName;
        this.avatarUrl = avatarUrl;
        this.bio = bio;
        this.wallpaperUrl = wallpaperUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getWallpaperUrl() {
        return wallpaperUrl;
    }

    public void setWallpaperUrl(String wallpaperUrl) {
        this.wallpaperUrl = wallpaperUrl;
    }
}
