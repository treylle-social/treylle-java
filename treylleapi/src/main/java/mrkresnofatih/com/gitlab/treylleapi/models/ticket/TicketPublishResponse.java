package mrkresnofatih.com.gitlab.treylleapi.models.ticket;

public class TicketPublishResponse {
    private String message;

    public TicketPublishResponse() {
        this.message = "Ticket Successfully Published";
    }

    public TicketPublishResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
