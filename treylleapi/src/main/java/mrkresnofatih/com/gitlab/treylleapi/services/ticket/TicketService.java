package mrkresnofatih.com.gitlab.treylleapi.services.ticket;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketPublishRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketPublishResponse;

public interface TicketService {
    FuncResponse<TicketPublishResponse> publishTicket(TicketPublishRequest publishRequest);
}
