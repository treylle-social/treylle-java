package mrkresnofatih.com.gitlab.treylleapi.services.followings;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.*;

public interface FollowingService {
    FuncResponse<FollowingCreateResponse> create(FollowingCreateRequest createRequest);
    FuncResponse<FollowingGetResponse> get(FollowingGetRequest getRequest);
    FuncResponse<FollowingListResponse> list(FollowingListRequest listRequest);
    FuncResponse<FollowingDeleteResponse> delete(FollowingDeleteRequest deleteRequest);
}
