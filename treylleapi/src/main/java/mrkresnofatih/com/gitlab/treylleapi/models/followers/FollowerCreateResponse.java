package mrkresnofatih.com.gitlab.treylleapi.models.followers;

public class FollowerCreateResponse {
    private String message;

    public FollowerCreateResponse() {
        this.message = "Create Follower Success";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
