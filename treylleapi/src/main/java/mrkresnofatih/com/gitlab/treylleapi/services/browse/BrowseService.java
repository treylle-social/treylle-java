package mrkresnofatih.com.gitlab.treylleapi.services.browse;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.browse.BrowseUserSearchRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.browse.BrowseUserSearchResponse;

public interface BrowseService {
    FuncResponse<BrowseUserSearchResponse> searchUser(BrowseUserSearchRequest userSearchRequest);
}
