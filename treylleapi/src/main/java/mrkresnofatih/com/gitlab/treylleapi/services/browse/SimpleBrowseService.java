package mrkresnofatih.com.gitlab.treylleapi.services.browse;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.browse.BrowseUserGetResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.browse.BrowseUserSearchRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.browse.BrowseUserSearchResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserListRequest;
import mrkresnofatih.com.gitlab.treylleapi.services.users.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class SimpleBrowseService implements BrowseService {
    private final Logger logger;
    private final UserService userService;

    @Autowired
    public SimpleBrowseService(UserService userService) {
        this.userService = userService;
        this.logger = LoggerFactory.getLogger(SimpleBrowseService.class);
    }

    @Override
    public FuncResponse<BrowseUserSearchResponse> searchUser(BrowseUserSearchRequest userSearchRequest) {
        logger.info("start searchUser w. data: {}", userSearchRequest.toJsonSerialized());
        var userListRequest = new UserListRequest(
                userSearchRequest.getQuery(),
                userSearchRequest.getLastResult(),
                userSearchRequest.getPageSize());
        var userListResult = userService.list(userListRequest);
        if (userListResult.isError()) {
            logger.error("failed to list users");
            return new FuncResponse<>("failed to list users");
        }
        var list = new ArrayList<BrowseUserGetResponse>();
        for (var user : userListResult.getData().getUsers()) {
            var newUser = new BrowseUserGetResponse();
            newUser.setUsername(user.getUsername());
            newUser.setFullName(user.getFullName());
            newUser.setAvatarUrl(user.getAvatarUrl());
            newUser.setBio(user.getBio());
            list.add(newUser);
        }
        logger.info("finished searchUser");
        return new FuncResponse<>(new BrowseUserSearchResponse(list));
    }
}
