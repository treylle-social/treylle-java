package mrkresnofatih.com.gitlab.treylleapi.models.browse;

import java.util.ArrayList;
import java.util.List;

public class BrowseUserSearchResponse {
    private List<BrowseUserGetResponse> users;

    public BrowseUserSearchResponse() {
        this.users = new ArrayList<>();
    }

    public BrowseUserSearchResponse(List<BrowseUserGetResponse> users) {
        this.users = users;
    }

    public List<BrowseUserGetResponse> getUsers() {
        return users;
    }

    public void setUsers(List<BrowseUserGetResponse> users) {
        this.users = users;
    }
}
