package mrkresnofatih.com.gitlab.treylleapi.services.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import mrkresnofatih.com.gitlab.treylleapi.configurations.TreylleConfig;
import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtCreateResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtValidateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtValidateResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Objects;

@Service
public class Auth0JwtServiceImpl implements JwtService {
    private final String secret;
    private final String issuer;
    private final String EXPIRES_AT_CLAIM_KEY = "X-Expires-At";
    private final String TOKEN_ISSUER_CLAIM_KEY = "X-Token-Issuer";
    private final Logger logger;

    @Autowired
    public Auth0JwtServiceImpl(TreylleConfig treylleConfig) {
        this.logger = LoggerFactory.getLogger(Auth0JwtServiceImpl.class);
        this.secret = treylleConfig.getAuthSecret();
        this.issuer = treylleConfig.getAuthIssuer();
    }

    @Override
    public FuncResponse<JwtCreateResponse> create(JwtCreateRequest createRequest) {
        logger.info("start create jwt w. data: {}", createRequest.toJsonSerialized());
        try {
            var token = JWT.create()
                    .withPayload(createRequest.getClaims())
                    .withClaim(TOKEN_ISSUER_CLAIM_KEY, issuer)
                    .withClaim(EXPIRES_AT_CLAIM_KEY, _getUtcExpiresAfter(createRequest.getLifeTime()))
                    .sign(_getAlgorithm());
            logger.info("finish create jwt token");
            return new FuncResponse<>(new JwtCreateResponse(token));
        } catch (Exception e) {
            logger.error("Failed to create JWT with error: {}", e.getMessage());
            return new FuncResponse<>("create Jwt Failed");
        }
    }

    @Override
    public FuncResponse<JwtValidateResponse> validate(JwtValidateRequest validateRequest) {
        logger.info("start validate jwt w. data: {}", validateRequest.toJsonSerialized());
        try {
            var verification = JWT.require(_getAlgorithm())
                    .withClaim(TOKEN_ISSUER_CLAIM_KEY, issuer)
                    .withClaimPresence(EXPIRES_AT_CLAIM_KEY);
            if (!Objects.isNull(validateRequest.getValidationClaims())) {
                var map = validateRequest.getValidationClaims();
                for (var key : map.keySet()) {
                    verification = verification.withClaim(key, map.get(key));
                }
            }
            if (!Objects.isNull(validateRequest.getExtractionClaims())) {
                var set = validateRequest.getExtractionClaims();
                for (var key : set) {
                    verification.withClaimPresence(key);
                }
            }
            var jwtVerifier = verification.build();
            var decoded = jwtVerifier.verify(validateRequest.getToken());
            if (decoded.getClaim(EXPIRES_AT_CLAIM_KEY).asLong() < _getUtcNow()) {
                throw new Exception("expired_at claim key of token is expired");
            }
            var outputClaims = new HashMap<String, String>();
            if (!Objects.isNull(validateRequest.getExtractionClaims())) {
                for (var claimType : validateRequest.getExtractionClaims()) {
                    if (!decoded.getClaim(claimType).isMissing() || !decoded.getClaim(claimType).isNull()) {
                        var claimValue = decoded.getClaim(claimType).asString();
                        outputClaims.put(claimType, claimValue);
                    }
                }
            }
            logger.info("finish validate jwt");
            return new FuncResponse<>(new JwtValidateResponse(true, outputClaims));
        } catch (Exception e) {
            logger.error("Failed to validate Jwt with error: {}", e.getMessage());
            return new FuncResponse<>("Validate Jwt Failed");
        }
    }

    private Algorithm _getAlgorithm() {
        return Algorithm.HMAC256(secret);
    }

    private Long _getUtcExpiresAfter(Long lifeTime) {
        if (lifeTime <= 0) {
            throw new RuntimeException("lifetime requested invalid: should be greater than 0 (zero)");
        }
        return System.currentTimeMillis() + lifeTime * 1000;
    }

    private Long _getUtcNow() {
        return System.currentTimeMillis();
    }
}
