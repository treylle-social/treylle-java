package mrkresnofatih.com.gitlab.treylleapi.services.auth;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.*;

public interface AuthService {
    FuncResponse<AuthLoginResponse> login(AuthLoginRequest loginRequest);
    FuncResponse<AuthSignupCheckResponse> checkSignup(AuthSignupCheckRequest signupCheckRequest);
    FuncResponse<AuthSignupResponse> signup(AuthSignupRequest signupRequest);
    FuncResponse<AuthPermitResponse> permit(AuthPermitRequest permitRequest);
}
