package mrkresnofatih.com.gitlab.treylleapi.models.follow;

public class FollowStartResponse {
    private String message;

    public FollowStartResponse() {
        this.message = "Finished Start Follow";
    }

    public FollowStartResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
