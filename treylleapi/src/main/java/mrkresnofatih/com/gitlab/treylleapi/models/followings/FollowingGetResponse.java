package mrkresnofatih.com.gitlab.treylleapi.models.followings;

public class FollowingGetResponse {
    private String username;
    private String followingUsername;

    public FollowingGetResponse() {
    }

    public FollowingGetResponse(String username, String followingUsername) {
        this.username = username;
        this.followingUsername = followingUsername;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFollowingUsername() {
        return followingUsername;
    }

    public void setFollowingUsername(String followingUsername) {
        this.followingUsername = followingUsername;
    }
}
