package mrkresnofatih.com.gitlab.treylleapi.services.followers;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;

import java.util.ArrayList;
import java.util.Objects;

@Service
public class DynamoFollowerServiceImpl implements FollowerService {
    private final Logger logger;
    private final DynamoDbTable<DynamoFollowerEntityImpl> dynamoFollowerEntityDynamoDbTable;

    @Autowired
    public DynamoFollowerServiceImpl(DynamoDbTable<DynamoFollowerEntityImpl> dynamoFollowerEntityDynamoDbTable) {
        this.dynamoFollowerEntityDynamoDbTable = dynamoFollowerEntityDynamoDbTable;
        this.logger = LoggerFactory.getLogger(DynamoFollowerServiceImpl.class);
    }

    private String getPartitionKey(String username) {
        return String.format("Followers_%s", username);
    }

    private String getSortKey(String followerUsername) {
        return String.format("Follower_%s", followerUsername);
    }

    @Override
    public FuncResponse<FollowerCreateResponse> create(FollowerCreateRequest createRequest) {
        logger.info("start create follower w. data: {}", createRequest.toJsonSerialized());
        try {
            var newFollower = new DynamoFollowerEntityImpl();
            newFollower.setPartitionKey(getPartitionKey(createRequest.getUsername()));
            newFollower.setSortKey(getSortKey(createRequest.getFollowerUsername()));
            newFollower.setFollowerUsername(createRequest.getFollowerUsername());
            newFollower.setUsername(createRequest.getUsername());

            dynamoFollowerEntityDynamoDbTable.putItem(newFollower);
            var response = new FollowerCreateResponse();
            logger.info("success create follower");
            return new FuncResponse<>(response);
        } catch (Exception e) {
            logger.error("create follower failed, error: {}", e.getMessage());
            return new FuncResponse<>("failed to create follower");
        }
    }

    @Override
    public FuncResponse<FollowerGetResponse> get(FollowerGetRequest getRequest) {
        logger.info("start get follower w. data: {}", getRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey(getRequest.getUsername());
            var sortKey = getSortKey(getRequest.getFollowerUsername());
            var followerKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var followerFound = dynamoFollowerEntityDynamoDbTable.getItem(followerKey);
            if (Objects.isNull(followerFound)) {
                throw new Exception("Follower not found");
            }
            var followerGetResponse = new FollowerGetResponse();
            followerGetResponse.setUsername(followerFound.getUsername());
            followerGetResponse.setFollowerUsername(followerFound.getFollowerUsername());
            logger.info("Get follower success");
            return new FuncResponse<>(followerGetResponse);
        } catch (Exception e) {
            logger.error("get follower failed: {}", e.getMessage());
            return new FuncResponse<>("get follower failed");
        }
    }

    @Override
    public FuncResponse<FollowerListResponse> list(FollowerListRequest listRequest) {
        logger.info("start list follower w. data: {}", listRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey(listRequest.getUsername());
            var sortKey = getSortKey("!");
            if (!Objects.isNull(listRequest.getLastResult())) {
                sortKey = getSortKey(listRequest.getLastResult());
            }
            var startKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var query = QueryEnhancedRequest.builder()
                    .limit(listRequest.getPageSize())
                    .queryConditional(QueryConditional.sortGreaterThan(startKey))
                    .build();
            var result = dynamoFollowerEntityDynamoDbTable
                    .query(query)
                    .items()
                    .iterator();
            var followerList = new ArrayList<FollowerGetResponse>();
            FollowerGetResponse followerDetail;
            while (result.hasNext()) {
                followerDetail = new FollowerGetResponse();
                var record = result.next();
                followerDetail.setUsername(record.getUsername());
                followerDetail.setFollowerUsername(record.getFollowerUsername());
                followerList.add(followerDetail);
            }
            var response = new FollowerListResponse(followerList);
            logger.info("finish list followers");
            return new FuncResponse<>(response);
        } catch (Exception e) {
            logger.error("list follower failed: {}", e.getMessage());
            return new FuncResponse<>("list follower failed");
        }
    }

    @Override
    public FuncResponse<FollowerDeleteResponse> delete(FollowerDeleteRequest deleteRequest) {
        logger.info("start delete follower w. data: {}", deleteRequest.toJsonSerialized());
        try {
            var partitionKey = getPartitionKey(deleteRequest.getUsername());
            var sortKey = getSortKey(deleteRequest.getFollowerUsername());
            var followerKey = Key.builder()
                    .partitionValue(partitionKey)
                    .sortValue(sortKey)
                    .build();
            var followerFound = dynamoFollowerEntityDynamoDbTable.getItem(followerKey);
            if (Objects.isNull(followerFound)) {
                throw new Exception("Follower not found");
            }
            dynamoFollowerEntityDynamoDbTable.deleteItem(followerKey);
            logger.error("success: delete user");
            return new FuncResponse<>(new FollowerDeleteResponse());
        } catch (Exception e) {
            logger.error("delete follower failed: {}", e.getMessage());
            return new FuncResponse<>("delete follower failed");
        }
    }
}
