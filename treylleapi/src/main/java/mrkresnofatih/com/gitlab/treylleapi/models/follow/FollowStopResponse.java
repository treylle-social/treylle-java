package mrkresnofatih.com.gitlab.treylleapi.models.follow;

public class FollowStopResponse {
    private String message;

    public FollowStopResponse() {
        this.message = "Unfollow success";
    }

    public FollowStopResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
