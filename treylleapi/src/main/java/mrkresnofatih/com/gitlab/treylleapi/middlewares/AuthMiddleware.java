package mrkresnofatih.com.gitlab.treylleapi.middlewares;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.AuthPermitRequest;
import mrkresnofatih.com.gitlab.treylleapi.services.auth.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
@Order(1)
public class AuthMiddleware implements Filter {
    private final Logger logger;
    private final AuthService authService;

    @Autowired
    public AuthMiddleware(AuthService authService) {
        this.authService = authService;
        this.logger = LoggerFactory.getLogger(AuthMiddleware.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        var req = (HttpServletRequest) servletRequest;
        var uri = req.getRequestURI();
        var isAuthRequired = _isRequireAuth(uri);
        if (isAuthRequired) {
            logger.debug("isAuthRequired: True, will proceed to authorization");
            var token = req.getHeader("Authorization");
            if (token != null && !token.isEmpty()) {
                logger.debug("authorization header found! will continue to assess token");
                var validationResult = authService
                        .permit(new AuthPermitRequest(token));
                if (!validationResult.isError() && validationResult.getData().isPermitted()) {
                    logger.debug("validation is valid");
                    var mutableRequest = new MutableHttpServletRequest(req);
                    var username = validationResult.getData().getUsername();
                    var tokenId = validationResult.getData().getTokenId();
                    var claimHeaders = new HashMap<String, String>();
                    claimHeaders.put("Claims-username", username);
                    claimHeaders.put("Claims-tokenId", tokenId);
                    for (var claim : claimHeaders.keySet()) {
                        mutableRequest.putHeader(claim, claimHeaders.get(claim));
                    }
                    filterChain.doFilter(mutableRequest, servletResponse);
                    return;
                }
            }
            Map<String, Object> response = new HashMap<>();
            response.put("errorMessage", "Unauthorized");
            response.put("data", null);

            var httpServletResponse = (HttpServletResponse) servletResponse;
            httpServletResponse.setStatus(HttpStatus.FORBIDDEN.value());
            httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
            var mapper = new ObjectMapper();
            mapper.writeValue(httpServletResponse.getWriter(), response);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private boolean _isRequireAuth(String path) {
        var prefixList = new ArrayList<String>();
        prefixList.add("/api/v1/profile");
        prefixList.add("/api/v1/follow");
        prefixList.add("/api/v1/browse");

        for (var pref : prefixList) {
            if (path.startsWith(pref)) {
                return true;
            }
        }
        return false;
    }
}
