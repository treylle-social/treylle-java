package mrkresnofatih.com.gitlab.treylleapi.services.auth;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.AuthLoginRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.AuthPermitRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.AuthSignupCheckRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.auth.AuthSignupRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtCreateResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtValidateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtValidateResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.users.*;
import mrkresnofatih.com.gitlab.treylleapi.services.hash.HashService;
import mrkresnofatih.com.gitlab.treylleapi.services.jwt.JwtService;
import mrkresnofatih.com.gitlab.treylleapi.services.users.UserService;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
class SimpleAuthServiceTest {
    @Mock
    private JwtService jwtService;

    @Mock
    private HashService hashService;

    @Mock
    private UserService userService;

    private SimpleAuthService authService;

    @BeforeEach
    void setUp() {
        authService = new SimpleAuthService(jwtService, hashService, userService);
    }

    @Test
    void login_when_userService_get_failed_should_return_error() {
        var loginRequest = new AuthLoginRequest("kevin.durant", "passWord123#");
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get user"));
        var result = authService.login(loginRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void login_when_hashService_compare_failed_should_return_error() {
        var loginRequest = new AuthLoginRequest("kevin.durant", "passWord123#");
        var mockResponse = new UserGetResponse();
        mockResponse.setUsername(loginRequest.getUsername());
        mockResponse.setPassword("someHashedPassword");
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(mockResponse));
        Mockito.when(hashService.compare(Mockito.any(String.class), Mockito.any(String.class)))
                .thenReturn(new FuncResponse<>("failed to compare hash"));
        var result = authService.login(loginRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void login_when_hashService_compare_invalid_should_return_error() {
        var loginRequest = new AuthLoginRequest("kevin.durant", "passWord123#");
        var mockResponse = new UserGetResponse();
        mockResponse.setUsername(loginRequest.getUsername());
        mockResponse.setPassword("someHashedPassword");
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(mockResponse));
        Mockito.when(hashService.compare(Mockito.any(String.class), Mockito.any(String.class)))
                .thenReturn(new FuncResponse<>(false));
        var result = authService.login(loginRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void login_when_jwtService_create_failed_should_return_error() {
        var loginRequest = new AuthLoginRequest("kevin.durant", "passWord123#");
        var mockResponse = new UserGetResponse();
        mockResponse.setUsername(loginRequest.getUsername());
        mockResponse.setPassword("someHashedPassword");
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(mockResponse));
        Mockito.when(hashService.compare(Mockito.any(String.class), Mockito.any(String.class)))
                .thenReturn(new FuncResponse<>(true));
        Mockito.when(jwtService.create(Mockito.any(JwtCreateRequest.class)))
                .thenReturn(new FuncResponse<>("failed to create jwt token"));
        var result = authService.login(loginRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void login_when_jwtService_create_success_should_return_success() {
        var loginRequest = new AuthLoginRequest("kevin.durant", "passWord123#");
        var mockResponse = new UserGetResponse();
        mockResponse.setUsername(loginRequest.getUsername());
        mockResponse.setPassword("someHashedPassword");
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(mockResponse));
        Mockito.when(hashService.compare(Mockito.any(String.class), Mockito.any(String.class)))
                .thenReturn(new FuncResponse<>(true));
        Mockito.when(jwtService.create(Mockito.any(JwtCreateRequest.class)))
                .thenReturn(new FuncResponse<>(new JwtCreateResponse("someToken")));
        var result = authService.login(loginRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void checkSignup_when_userService_list_failed_should_return_failed() {
        var checkSignupRequest = new AuthSignupCheckRequest("kevin.durant");
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>("failed to list users"));
        var result = authService.checkSignup(checkSignupRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void checkSignup_when_userService_list_userFound_should_return_success() {
        var checkSignupRequest = new AuthSignupCheckRequest("kevin.durant");
        var mockResultList = new ArrayList<UserGetResponse>();
        var mockUser = new UserGetResponse();
        mockUser.setUsername(checkSignupRequest.getUsername());
        mockResultList.add(mockUser);
        var mockResult = new UserListResponse(mockResultList);
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>(mockResult));
        var result = authService.checkSignup(checkSignupRequest);
        SoftAssertions assertBundle = new SoftAssertions();
        assertBundle.assertThat(result.isError()).isFalse();
        assertBundle.assertThat(result.getData().isUserNameTaken()).isTrue();
        assertBundle.assertAll();
    }

    @Test
    void checkSignup_when_userService_list_userNotFound_should_return_success() {
        var checkSignupRequest = new AuthSignupCheckRequest("kevin.durant");
        var mockResultList = new ArrayList<UserGetResponse>();
        var mockResult = new UserListResponse(mockResultList);
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>(mockResult));
        var result = authService.checkSignup(checkSignupRequest);
        SoftAssertions assertBundle = new SoftAssertions();
        assertBundle.assertThat(result.isError()).isFalse();
        assertBundle.assertThat(result.getData().isUserNameTaken()).isFalse();
        assertBundle.assertAll();
    }

    @Test
    void signup_when_checkSignup_fail_should_return_error() {
        var signupRequest = new AuthSignupRequest(
                "kevin.durant",
                "passWord123#",
                "Kevin Durant");
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>("failed to list users"));
        var result = authService.signup(signupRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void signup_when_checkSignup_usernameTaken_should_return_error() {
        var signupRequest = new AuthSignupRequest(
                "kevin.durant",
                "passWord123#",
                "Kevin Durant");
        var mockResultList = new ArrayList<UserGetResponse>();
        var mockUser = new UserGetResponse();
        mockUser.setUsername(signupRequest.getUsername());
        mockResultList.add(mockUser);
        var mockResult = new UserListResponse(mockResultList);
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>(mockResult));
        var result = authService.signup(signupRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void signup_when_hashService_hash_failed_should_return_error() {
        var signupRequest = new AuthSignupRequest(
                "kevin.durant",
                "passWord123#",
                "Kevin Durant");
        var mockResultList = new ArrayList<UserGetResponse>();
        var mockResult = new UserListResponse(mockResultList);
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>(mockResult));
        Mockito.when(hashService.hash(Mockito.any(String.class)))
                .thenReturn(new FuncResponse<>("failed to hash password"));
        var result = authService.signup(signupRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void signup_when_userService_create_failed_should_return_error() {
        var signupRequest = new AuthSignupRequest(
                "kevin.durant",
                "passWord123#",
                "Kevin Durant");
        var mockResultList = new ArrayList<UserGetResponse>();
        var mockResult = new UserListResponse(mockResultList);
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>(mockResult));
        Mockito.when(hashService.hash(Mockito.any(String.class)))
                .thenReturn(new FuncResponse<>("hashedPassword", ""));
        Mockito.when(userService.create(Mockito.any(UserCreateRequest.class)))
                .thenReturn(new FuncResponse<>("failed to create new user"));
        var result = authService.signup(signupRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void signup_when_userService_create_success_should_return_success() {
        var signupRequest = new AuthSignupRequest(
                "kevin.durant",
                "passWord123#",
                "Kevin Durant");
        var mockResultList = new ArrayList<UserGetResponse>();
        var mockResult = new UserListResponse(mockResultList);
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>(mockResult));
        Mockito.when(hashService.hash(Mockito.any(String.class)))
                .thenReturn(new FuncResponse<>("hashedPassword", ""));
        Mockito.when(userService.create(Mockito.any(UserCreateRequest.class)))
                .thenReturn(new FuncResponse<>(new UserCreateResponse()));
        var result = authService.signup(signupRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void permit_when_jwtService_validate_fail_should_return_error() {
        var permitRequest = new AuthPermitRequest("token");
        Mockito.when(jwtService.validate(Mockito.any(JwtValidateRequest.class)))
                .thenReturn(new FuncResponse<>("failed to validate"));
        var result = authService.permit(permitRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void permit_when_jwtService_validate_invalid_should_return_error() {
        var permitRequest = new AuthPermitRequest("token");
        Mockito.when(jwtService.validate(Mockito.any(JwtValidateRequest.class)))
                .thenReturn(new FuncResponse<>(new JwtValidateResponse(false, new HashMap<>())));
        var result = authService.permit(permitRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void permit_when_jwtService_validate_valid_should_return_success() {
        var permitRequest = new AuthPermitRequest("token");
        var mockExtractedClaims = new HashMap<String, String>();
        mockExtractedClaims.put("X-Username", "kevin.durant");
        mockExtractedClaims.put("X-User-Token-Id", UUID.randomUUID().toString());
        Mockito.when(jwtService.validate(Mockito.any(JwtValidateRequest.class)))
                .thenReturn(new FuncResponse<>(new JwtValidateResponse(true, mockExtractedClaims)));
        var result = authService.permit(permitRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }
}
