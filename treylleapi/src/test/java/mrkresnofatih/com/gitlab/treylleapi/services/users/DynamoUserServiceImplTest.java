package mrkresnofatih.com.gitlab.treylleapi.services.users;

import mrkresnofatih.com.gitlab.treylleapi.models.users.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;

@ExtendWith(MockitoExtension.class)
class DynamoUserServiceImplTest {
    @Mock
    private DynamoDbTable<DynamoUserEntityImpl> dynamoUserEntityDynamoDbTable;

    private DynamoUserServiceImpl dynamoUserService;

    @BeforeEach
    void setUp() {
        dynamoUserService = new DynamoUserServiceImpl(dynamoUserEntityDynamoDbTable);
    }

    private UserCreateRequest getUserCreateRequest() {
        var req = new UserCreateRequest();
        req.setUsername("kevin.durant");
        req.setFullName("Kevin Durant");
        req.setAvatarUrl("someAvatarUrl");
        req.setBio("someBio");
        req.setJoinedAt("June 2020");
        req.setWallpaperUrl("someWallpaperUrl");
        req.setPassword("somePassword");
        return req;
    }

    private UserUpdateRequest getUserUpdateRequest() {
        var req = new UserUpdateRequest();
        req.setUsername("kevin.durant");
        req.setFullName("Kevin Durant");
        req.setAvatarUrl("someAvatarUrl");
        req.setBio("someBio");
        req.setJoinedAt("June 2020");
        req.setWallpaperUrl("someWallpaperUrl");
        req.setPassword("somePassword");
        return req;
    }

    private UserGetRequest getUserGetRequest() {
        var req = new UserGetRequest();
        req.setUsername("kevin.durant");
        return req;
    }

    private UserListRequest getUserListRequest() {
        var req = new UserListRequest();
        req.setSearch("kevin.durant");
        req.setLastResult("kevin.durant123");
        req.setPageSize(25);
        return req;
    }

    private UserDeleteRequest getUserDeleteRequest() {
        var req = new UserDeleteRequest();
        req.setUsername("username");
        return req;
    }

    @Test
    void create_when_putItem_throws_should_return_error() {
        var createRequest = getUserCreateRequest();
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoUserEntityDynamoDbTable)
                .putItem(Mockito.any(DynamoUserEntityImpl.class));
        var result = dynamoUserService.create(createRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void create_when_putItem_notThrow_should_return_success() {
        var createRequest = getUserCreateRequest();
        var result = dynamoUserService.create(createRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void get_when_getItem_null_should_return_error() {
        var getRequest = getUserGetRequest();
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var result = dynamoUserService.get(getRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void get_when_getItem_notNull_should_return_success() {
        var getRequest = getUserGetRequest();
        var mockGetResponse = new DynamoUserEntityImpl();
        mockGetResponse.setUsername("username");
        mockGetResponse.setAvatarUrl("avatar");
        mockGetResponse.setBio("bio");
        mockGetResponse.setJoinedAt("april 2020");
        mockGetResponse.setPassword("password");
        mockGetResponse.setWallpaperUrl("wallpaper");
        mockGetResponse.setFullName("fullName");
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockGetResponse);
        var result = dynamoUserService.get(getRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void list_when_query_throw_should_return_error() {
        var listRequest = getUserListRequest();
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoUserEntityDynamoDbTable)
                .query(Mockito.any(QueryEnhancedRequest.class));
        var result = dynamoUserService.list(listRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void update_when_getItem_null_should_return_error() {
        var updateRequest = getUserUpdateRequest();
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var result = dynamoUserService.update(updateRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void update_when_updateItem_throw_should_return_error() {
        var updateRequest = getUserUpdateRequest();
        var mockGetResponse = new DynamoUserEntityImpl();
        mockGetResponse.setUsername("username");
        mockGetResponse.setAvatarUrl("avatar");
        mockGetResponse.setBio("bio");
        mockGetResponse.setJoinedAt("april 2020");
        mockGetResponse.setPassword("password");
        mockGetResponse.setWallpaperUrl("wallpaper");
        mockGetResponse.setFullName("fullName");
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockGetResponse);
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoUserEntityDynamoDbTable)
                .updateItem(Mockito.any(DynamoUserEntityImpl.class));
        var result = dynamoUserService.update(updateRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void update_when_updateItem_notThrow_should_return_success() {
        var updateRequest = getUserUpdateRequest();
        var mockGetResponse = new DynamoUserEntityImpl();
        mockGetResponse.setUsername("username");
        mockGetResponse.setAvatarUrl("avatar");
        mockGetResponse.setBio("bio");
        mockGetResponse.setJoinedAt("april 2020");
        mockGetResponse.setPassword("password");
        mockGetResponse.setWallpaperUrl("wallpaper");
        mockGetResponse.setFullName("fullName");
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockGetResponse);
        var result = dynamoUserService.update(updateRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void delete_when_getItem_null_should_return_error() {
        var deleteRequest = getUserDeleteRequest();
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var result = dynamoUserService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void delete_when_deleteItem_throws_should_return_error() {
        var deleteRequest = getUserDeleteRequest();
        var mockGetResponse = new DynamoUserEntityImpl();
        mockGetResponse.setUsername("username");
        mockGetResponse.setAvatarUrl("avatar");
        mockGetResponse.setBio("bio");
        mockGetResponse.setJoinedAt("april 2020");
        mockGetResponse.setPassword("password");
        mockGetResponse.setWallpaperUrl("wallpaper");
        mockGetResponse.setFullName("fullName");
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockGetResponse);
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoUserEntityDynamoDbTable)
                .deleteItem(Mockito.any(Key.class));
        var result = dynamoUserService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void delete_when_deleteItem_notThrow_should_return_success() {
        var deleteRequest = getUserDeleteRequest();
        var mockGetResponse = new DynamoUserEntityImpl();
        mockGetResponse.setUsername("username");
        mockGetResponse.setAvatarUrl("avatar");
        mockGetResponse.setBio("bio");
        mockGetResponse.setJoinedAt("april 2020");
        mockGetResponse.setPassword("password");
        mockGetResponse.setWallpaperUrl("wallpaper");
        mockGetResponse.setFullName("fullName");
        Mockito.when(dynamoUserEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockGetResponse);
        var result = dynamoUserService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }
}