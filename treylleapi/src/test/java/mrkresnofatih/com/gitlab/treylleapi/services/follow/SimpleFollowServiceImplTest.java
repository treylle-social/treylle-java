package mrkresnofatih.com.gitlab.treylleapi.services.follow;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.FollowCheckIsFollowerRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.FollowCheckIsFollowingRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.FollowStartRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.follow.FollowStopRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.FollowerCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.FollowerDeleteRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.FollowerGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followers.FollowerGetResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.FollowingCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.FollowingDeleteRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.FollowingGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.followings.FollowingGetResponse;
import mrkresnofatih.com.gitlab.treylleapi.services.followers.FollowerService;
import mrkresnofatih.com.gitlab.treylleapi.services.followings.FollowingService;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SimpleFollowServiceImplTest {
    @Mock
    private FollowerService followerService;
    @Mock
    private FollowingService followingService;
    private SimpleFollowServiceImpl followService;

    @BeforeEach
    void setUp() {
        followService = new SimpleFollowServiceImpl(followerService, followingService);
    }

    @Test
    void startFollow_when_followerGet_and_followingGet_success_should_return_success() {
        var startFollowRequest = new FollowStartRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>(
                        new FollowerGetResponse(
                                startFollowRequest.getUsernameToFollow(),
                                startFollowRequest.getUsername())));
        Mockito.when(followingService.get(Mockito.any(FollowingGetRequest.class)))
                .thenReturn(new FuncResponse<>(
                        new FollowingGetResponse(
                                startFollowRequest.getUsername(),
                                startFollowRequest.getUsernameToFollow())));
        var result = followService.startFollow(startFollowRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void startFollow_when_followerCreate_failed_should_return_error() {
        var startFollowRequest = new FollowStartRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get follower data"));
        Mockito.when(followerService.create(Mockito.any(FollowerCreateRequest.class)))
                .thenReturn(new FuncResponse<>("failed to create follower data"));
        var result = followService.startFollow(startFollowRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void startFollow_when_followingCreate_failed_should_return_error() {
        var startFollowRequest = new FollowStartRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>(
                        new FollowerGetResponse(
                                startFollowRequest.getUsernameToFollow(),
                                startFollowRequest.getUsername())));
        Mockito.when(followingService.get(Mockito.any(FollowingGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get following data"));
        Mockito.when(followingService.create(Mockito.any(FollowingCreateRequest.class)))
                .thenReturn(new FuncResponse<>("failed to create following data"));
        var result = followService.startFollow(startFollowRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void stopFollow_when_followerGet_and_followingGet_failed_should_return_success() {
        var stopFollowRequest = new FollowStopRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get follower data"));
        Mockito.when(followingService.get(Mockito.any(FollowingGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get following data"));
        var result = followService.stopFollow(stopFollowRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void stopFollow_when_followerDelete_failed_should_return_success() {
        var stopFollowRequest = new FollowStopRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>(new FollowerGetResponse(stopFollowRequest.getUsernameToUnfollow(), stopFollowRequest.getUsername())));
        Mockito.when(followerService.delete(Mockito.any(FollowerDeleteRequest.class)))
                .thenReturn(new FuncResponse<>("failed to delete follower data"));
        var result = followService.stopFollow(stopFollowRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void stopFollow_when_followingDelete_failed_should_return_success() {
        var stopFollowRequest = new FollowStopRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get follower data"));
        Mockito.when(followingService.get(Mockito.any(FollowingGetRequest.class)))
                .thenReturn(new FuncResponse<>(new FollowingGetResponse(stopFollowRequest.getUsername(), stopFollowRequest.getUsernameToUnfollow())));
        Mockito.when(followingService.delete(Mockito.any(FollowingDeleteRequest.class)))
                .thenReturn(new FuncResponse<>("failed to delete following data"));
        var result = followService.stopFollow(stopFollowRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void checkIsFollower_when_followerGet_fail_should_return_success() {
        var checkRequest = new FollowCheckIsFollowerRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get follower data"));
        var result = followService.checkIsFollower(checkRequest);
        SoftAssertions assertBundle = new SoftAssertions();
        assertBundle.assertThat(result.isError()).isFalse();
        assertBundle.assertThat(result.getData().isFollower()).isFalse();
        assertBundle.assertAll();
    }

    @Test
    void checkIsFollower_when_followerGet_success_should_return_success() {
        var checkRequest = new FollowCheckIsFollowerRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>(new FollowerGetResponse(checkRequest.getUsername(), checkRequest.getUsernameCandidateFollower())));
        var result = followService.checkIsFollower(checkRequest);
        SoftAssertions assertBundle = new SoftAssertions();
        assertBundle.assertThat(result.isError()).isFalse();
        assertBundle.assertThat(result.getData().isFollower()).isTrue();
        assertBundle.assertAll();
    }

    @Test
    void checkIsFollowing_when_followerGet_fail_should_return_success() {
        var checkRequest = new FollowCheckIsFollowingRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get follower data"));
        var result = followService.checkIsFollowing(checkRequest);
        SoftAssertions assertBundle = new SoftAssertions();
        assertBundle.assertThat(result.isError()).isFalse();
        assertBundle.assertThat(result.getData().isFollowing()).isFalse();
        assertBundle.assertAll();
    }

    @Test
    void checkIsFollowing_when_followerGet_success_should_return_success() {
        var checkRequest = new FollowCheckIsFollowingRequest("kevin.durant", "lebron.james");
        Mockito.when(followerService.get(Mockito.any(FollowerGetRequest.class)))
                .thenReturn(new FuncResponse<>(new FollowerGetResponse(checkRequest.getUsernameCandidateFollowing(), checkRequest.getUsername())));
        var result = followService.checkIsFollowing(checkRequest);
        SoftAssertions assertBundle = new SoftAssertions();
        assertBundle.assertThat(result.isError()).isFalse();
        assertBundle.assertThat(result.getData().isFollowing()).isTrue();
        assertBundle.assertAll();
    }
}