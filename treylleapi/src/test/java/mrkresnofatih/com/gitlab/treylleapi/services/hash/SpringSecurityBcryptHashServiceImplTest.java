package mrkresnofatih.com.gitlab.treylleapi.services.hash;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SpringSecurityBcryptHashServiceImplTest {
    private SpringSecurityBcryptHashServiceImpl hashService;

    @BeforeEach
    void setUp() {
        hashService = new SpringSecurityBcryptHashServiceImpl();
    }

    @Test
    void hash_should_return_success() {
        var password = "mySecretPassword123456#";
        var result = hashService.hash(password);
        Assertions.assertThat(result.getData()).isNotEqualTo(password);
    }

    @Test
    void compare_with_same_data_should_return_success() {
        var password = "mySecretPassword123456#";
        var result = hashService.hash(password);
        var compareResult = hashService.compare(password, result.getData());
        Assertions.assertThat(compareResult.getData()).isTrue();
    }

    @Test
    void compare_with_different_data_should_return_error() {
        var password = "mySecretPassword123456#";
        var differentPassword = "myOtherDifferentPassword12345#";
        var result = hashService.hash(password);
        var compareResult = hashService.compare(differentPassword, result.getData());
        Assertions.assertThat(compareResult.getData()).isFalse();
    }

}