package mrkresnofatih.com.gitlab.treylleapi.services.jwt;

import mrkresnofatih.com.gitlab.treylleapi.configurations.TreylleConfig;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtCreateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.jwt.JwtValidateRequest;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.HashSet;

@ExtendWith(MockitoExtension.class)
class Auth0JwtServiceImplTest {
    private Auth0JwtServiceImpl jwtService;

    @BeforeEach
    void setUp() {
        var config = new TreylleConfig();
        config.setAuthIssuer("https://app.treylle-iVeVxd3GI7tjfEECP654.com");
        config.setAuthSecret("EovSaxI8skZomQMMTZZMgELPlX6GazHRQoxJhiUyHkUy5pBZTs6livZ2J2po");
        jwtService = new Auth0JwtServiceImpl(config);
    }

    @Test
    void create_when_throw_should_return_error() {
        // invalid create jwt request to simulate throw exception
        var createRequest = new JwtCreateRequest(new HashMap<>(), 0L);
        var result = jwtService.create(createRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void create_when_success_should_return_success() {
        var claims = new HashMap<String, String>();
        claims.put("dummy-claim-key-1", "dummy-claim-value-1");
        claims.put("dummy-claim-key-2", "dummy-claim-value-2");
        var createRequest = new JwtCreateRequest(claims, 3600L);
        var result = jwtService.create(createRequest);
        SoftAssertions assertBundle = new SoftAssertions();
        assertBundle.assertThat(result.isError()).isFalse();
        assertBundle.assertThat(result.getData().getToken()).isNotEmpty();
        assertBundle.assertAll();
    }

    @Test
    void validate_when_tokenClaim_notFound_should_return_error() {
        var claims = new HashMap<String, String>();
        claims.put("dummy-claim-key-1", "dummy-claim-value-1");
        claims.put("dummy-claim-key-2", "dummy-claim-value-2");
        var createRequest = new JwtCreateRequest(claims, 3600L);
        var createResult = jwtService.create(createRequest);
        var extractionClaims = new HashSet<String>();
        extractionClaims.add("dummy-claim-key-3");
        var validateRequest = new JwtValidateRequest(
                createResult.getData().getToken(),
                extractionClaims,
                null);
        var validationResult = jwtService.validate(validateRequest);
        Assertions.assertThat(validationResult.isError()).isTrue();
    }

    @Test
    void validate_when_tokenClaim_notValid_should_return_error() {
        var claims = new HashMap<String, String>();
        claims.put("dummy-claim-key-1", "dummy-claim-value-1");
        claims.put("dummy-claim-key-2", "dummy-claim-value-2");
        var createRequest = new JwtCreateRequest(claims, 3600L);
        var createResult = jwtService.create(createRequest);
        var validationClaims = new HashMap<String, String>();
        validationClaims.put("dummy-claim-key-1", "dummy-claim-value-3");
        var validateRequest = new JwtValidateRequest(
                createResult.getData().getToken(),
                null,
                validationClaims);
        var validationResult = jwtService.validate(validateRequest);
        Assertions.assertThat(validationResult.isError()).isTrue();
    }

    @Test
    void validate_when_token_expired_should_return_error() throws InterruptedException {
        var claims = new HashMap<String, String>();
        claims.put("dummy-claim-key-1", "dummy-claim-value-1");
        claims.put("dummy-claim-key-2", "dummy-claim-value-2");
        var createRequest = new JwtCreateRequest(claims, 1L);
        var createResult = jwtService.create(createRequest);
        Thread.sleep(3000);
        var validateRequest = new JwtValidateRequest(
                createResult.getData().getToken(),
                null,
                null);
        var validationResult = jwtService.validate(validateRequest);
        Assertions.assertThat(validationResult.isError()).isTrue();
    }

    @Test
    void validate_when_token_valid_should_return_success() {
        var claims = new HashMap<String, String>();
        claims.put("dummy-claim-key-1", "dummy-claim-value-1");
        claims.put("dummy-claim-key-2", "dummy-claim-value-2");
        var createRequest = new JwtCreateRequest(claims, 1L);
        var createResult = jwtService.create(createRequest);
        var validateRequest = new JwtValidateRequest(
                createResult.getData().getToken(),
                null,
                null);
        var validationResult = jwtService.validate(validateRequest);
        Assertions.assertThat(validationResult.isError()).isFalse();
    }
}