package mrkresnofatih.com.gitlab.treylleapi.services.profile;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.profile.ProfileUpdateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserGetRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserGetResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserUpdateRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserUpdateResponse;
import mrkresnofatih.com.gitlab.treylleapi.services.users.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SimpleProfileServiceImplTest {
    @Mock
    private UserService userService;

    private SimpleProfileServiceImpl profileService;

    @BeforeEach
    void setUp() {
        profileService = new SimpleProfileServiceImpl(userService);
    }

    @Test
    void get_when_userGet_failed_should_return_error() {
        var getRequest = new ProfileGetRequest("kevin.durant");
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get user"));
        var result = profileService.get(getRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    private ProfileUpdateRequest getProfileUpdateRequest() {
        return new ProfileUpdateRequest(
                "kevin.durant",
                "Kevin Durant",
                "someAvatarUrl",
                "I'm me",
                "someWallpaperUrl"
        );
    }

    private UserGetResponse getUserGetResponse() {
        var userGetResponse = new UserGetResponse();
        userGetResponse.setUsername("kevin.durant");
        userGetResponse.setFullName("Kevin Durant");
        userGetResponse.setPassword("blablabla");
        userGetResponse.setWallpaperUrl("someWallpaperUrl");
        userGetResponse.setAvatarUrl("someAvatarUrl");
        userGetResponse.setJoinedAt("April 2020");
        userGetResponse.setBio("some bio");
        return userGetResponse;
    }

    @Test
    void get_when_userGet_success_should_return_success() {
        var getRequest = new ProfileGetRequest("kevin.durant");
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(getUserGetResponse()));
        var result = profileService.get(getRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void update_when_userGet_failed_should_return_error() {
        var updateRequest = getProfileUpdateRequest();
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>("failed to get user"));
        var result = profileService.update(updateRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void update_when_userUpdate_fail_should_return_error() {
        var updateRequest = getProfileUpdateRequest();
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(getUserGetResponse()));
        Mockito.when(userService.update(Mockito.any(UserUpdateRequest.class)))
                .thenReturn(new FuncResponse<>("failed to update user"));
        var result = profileService.update(updateRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void update_when_userUpdate_success_should_return_error() {
        var updateRequest = getProfileUpdateRequest();
        Mockito.when(userService.get(Mockito.any(UserGetRequest.class)))
                .thenReturn(new FuncResponse<>(getUserGetResponse()));
        Mockito.when(userService.update(Mockito.any(UserUpdateRequest.class)))
                .thenReturn(new FuncResponse<>(new UserUpdateResponse()));
        var result = profileService.update(updateRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }
}