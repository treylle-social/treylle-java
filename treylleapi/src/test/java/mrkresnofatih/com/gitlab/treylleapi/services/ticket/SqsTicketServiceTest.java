package mrkresnofatih.com.gitlab.treylleapi.services.ticket;

import com.amazonaws.services.sqs.model.AmazonSQSException;
import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import mrkresnofatih.com.gitlab.treylleapi.configurations.TreylleAwsResourceConfig;
import mrkresnofatih.com.gitlab.treylleapi.models.ticket.TicketPublishRequest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.Message;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
class SqsTicketServiceTest {
    @Mock
    private QueueMessagingTemplate queueMessagingTemplate;

    private SqsTicketService ticketService;

    @BeforeEach
    void setUp() {
        ticketService = new SqsTicketService(queueMessagingTemplate, new TreylleAwsResourceConfig("dynamodbtable", "sqsname"));
    }

    @Test
    void publishTicket_when_queueSend_failed_should_return_error() {
        var publishRequest = new TicketPublishRequest(new ArrayList<>(), "somedata");
        Mockito
                .doThrow(AmazonSQSException.class)
                .when(queueMessagingTemplate)
                .send(Mockito.any(String.class), Mockito.any(Message.class));
        var result = ticketService.publishTicket(publishRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void publishTicket_when_queueSend_success_should_return_success() {
        var publishRequest = new TicketPublishRequest(new ArrayList<>(), "somedata");
        var result = ticketService.publishTicket(publishRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

}