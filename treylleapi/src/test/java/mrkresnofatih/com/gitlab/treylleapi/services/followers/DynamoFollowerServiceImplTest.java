package mrkresnofatih.com.gitlab.treylleapi.services.followers;

import mrkresnofatih.com.gitlab.treylleapi.models.followers.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;

@ExtendWith(MockitoExtension.class)
class DynamoFollowerServiceImplTest {
    @Mock
    private DynamoDbTable<DynamoFollowerEntityImpl> dynamoFollowerEntityDynamoDbTable;

    private DynamoFollowerServiceImpl dynamoFollowerService;

    @BeforeEach
    void setUp() {
        dynamoFollowerService = new DynamoFollowerServiceImpl(dynamoFollowerEntityDynamoDbTable);
    }

    private FollowerCreateRequest getFollowerCreateRequest() {
        var req = new FollowerCreateRequest();
        req.setUsername("kevin.durant");
        req.setFollowerUsername("lebron.james");
        return req;
    }

    private FollowerGetRequest getFollowerGetRequest() {
        var req = new FollowerGetRequest();
        req.setUsername("kevin.durant");
        req.setFollowerUsername("lebron.james");
        return req;
    }

    private FollowerDeleteRequest getFollowerDeleteRequest() {
        var req = new FollowerDeleteRequest();
        req.setUsername("kevin.durant");
        req.setFollowerUsername("lebron.james");
        return req;
    }

    private FollowerListRequest getFollowerListRequest() {
        var req = new FollowerListRequest();
        req.setUsername("kevin.durant");
        req.setLastResult("kevin.durant123");
        req.setPageSize(20);
        return req;
    }

    @Test
    void create_when_putItem_throws_should_return_error() {
        var createRequest = getFollowerCreateRequest();
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoFollowerEntityDynamoDbTable)
                .putItem(Mockito.any(DynamoFollowerEntityImpl.class));
        var result = dynamoFollowerService.create(createRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void create_when_putItem_success_should_return_success() {
        var createRequest = getFollowerCreateRequest();
        var result = dynamoFollowerService.create(createRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void get_when_getItem_notFound_should_return_failed() {
        var getRequest = getFollowerGetRequest();
        Mockito.when(dynamoFollowerEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var result = dynamoFollowerService.get(getRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void get_when_getItem_success_should_return_success() {
        var getRequest = getFollowerGetRequest();
        var mockResponse = new DynamoFollowerEntityImpl();
        mockResponse.setUsername("kevin.durant");
        mockResponse.setFollowerUsername("lebron.james");
        Mockito.when(dynamoFollowerEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockResponse);
        var result = dynamoFollowerService.get(getRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void delete_when_notFound_should_return_failed() {
        var deleteRequest = getFollowerDeleteRequest();
        Mockito.when(dynamoFollowerEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var result = dynamoFollowerService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void delete_when_deleteItem_throw_should_return_failed() {
        var deleteRequest = getFollowerDeleteRequest();
        var mockResponse = new DynamoFollowerEntityImpl();
        mockResponse.setUsername("kevin.durant");
        mockResponse.setFollowerUsername("lebron.james");
        Mockito.when(dynamoFollowerEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockResponse);
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoFollowerEntityDynamoDbTable)
                .deleteItem(Mockito.any(Key.class));
        var result = dynamoFollowerService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void delete_when_deleteItem_notThrow_should_return_success() {
        var deleteRequest = getFollowerDeleteRequest();
        var mockResponse = new DynamoFollowerEntityImpl();
        mockResponse.setUsername("kevin.durant");
        mockResponse.setFollowerUsername("lebron.james");
        Mockito.when(dynamoFollowerEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockResponse);
        var result = dynamoFollowerService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void list_when_query_throw_should_return_error() {
        var listRequest = getFollowerListRequest();
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoFollowerEntityDynamoDbTable)
                .query(Mockito.any(QueryEnhancedRequest.class));
        var result = dynamoFollowerService.list(listRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }
}
