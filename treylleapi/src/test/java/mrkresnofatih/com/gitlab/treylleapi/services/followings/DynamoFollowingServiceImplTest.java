package mrkresnofatih.com.gitlab.treylleapi.services.followings;

import mrkresnofatih.com.gitlab.treylleapi.models.followings.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DynamoFollowingServiceImplTest {
    @Mock
    private DynamoDbTable<DynamoFollowingEntityImpl> dynamoFollowingEntityDynamoDbTable;

    private DynamoFollowingServiceImpl dynamoFollowingService;

    @BeforeEach
    void setUp() {
        dynamoFollowingService = new DynamoFollowingServiceImpl(dynamoFollowingEntityDynamoDbTable);
    }

    private FollowingCreateRequest getFollowingCreateRequest() {
        var req = new FollowingCreateRequest();
        req.setUsername("kevin.durant");
        req.setFollowingUsername("lebron.james");
        return req;
    }

    private FollowingGetRequest getFollowingGetRequest() {
        var req = new FollowingGetRequest();
        req.setUsername("kevin.durant");
        req.setFollowingUsername("lebron.james");
        return req;
    }

    private FollowingDeleteRequest getFollowingDeleteRequest() {
        return new FollowingDeleteRequest("kevin.durant", "lebron.james");
    }

    @Test
    void create_when_putItem_throws_should_return_error() {
        var createRequest = getFollowingCreateRequest();
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoFollowingEntityDynamoDbTable)
                .putItem(Mockito.any(DynamoFollowingEntityImpl.class));
        var result = dynamoFollowingService.create(createRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void create_when_putItem_success_should_return_success() {
        var createRequest = getFollowingCreateRequest();
        var result = dynamoFollowingService.create(createRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void get_when_getItem_notFound_should_return_failed() {
        var getRequest = getFollowingGetRequest();
        Mockito.when(dynamoFollowingEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var result = dynamoFollowingService.get(getRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void get_when_getItem_success_should_return_success() {
        var getRequest = getFollowingGetRequest();
        var mockResponse = new DynamoFollowingEntityImpl();
        mockResponse.setUsername(getRequest.getUsername());
        mockResponse.setFollowingUsername(getRequest.getFollowingUsername());
        Mockito.when(dynamoFollowingEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockResponse);
        var result = dynamoFollowingService.get(getRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }

    @Test
    void list_when_query_throw_should_return_error() {
        var listRequest = new FollowingListRequest("kevin.durant", null, 25);
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoFollowingEntityDynamoDbTable)
                .query(Mockito.any(QueryEnhancedRequest.class));
        var result = dynamoFollowingService.list(listRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void delete_when_getItem_notFound_should_return_error() {
        var deleteRequest = getFollowingDeleteRequest();
        Mockito.when(dynamoFollowingEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(null);
        var result = dynamoFollowingService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void delete_when_deleteItem_throws_should_return_error() {
        var deleteRequest = getFollowingDeleteRequest();
        var mockResponse = new DynamoFollowingEntityImpl();
        mockResponse.setUsername(deleteRequest.getUsername());
        mockResponse.setFollowingUsername(deleteRequest.getFollowingUsername());
        Mockito.when(dynamoFollowingEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockResponse);
        Mockito.doThrow(DynamoDbException.class)
                .when(dynamoFollowingEntityDynamoDbTable)
                .deleteItem(Mockito.any(Key.class));
        var result = dynamoFollowingService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void delete_when_deleteItem_success_should_return_success() {
        var deleteRequest = getFollowingDeleteRequest();
        var mockResponse = new DynamoFollowingEntityImpl();
        mockResponse.setUsername(deleteRequest.getUsername());
        mockResponse.setFollowingUsername(deleteRequest.getFollowingUsername());
        Mockito.when(dynamoFollowingEntityDynamoDbTable.getItem(Mockito.any(Key.class)))
                .thenReturn(mockResponse);
        var result = dynamoFollowingService.delete(deleteRequest);
        Assertions.assertThat(result.isError()).isFalse();
    }
}