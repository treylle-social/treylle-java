package mrkresnofatih.com.gitlab.treylleapi.services.browse;

import mrkresnofatih.com.gitlab.treylleapi.models.FuncResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.browse.BrowseUserSearchRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserGetResponse;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserListRequest;
import mrkresnofatih.com.gitlab.treylleapi.models.users.UserListResponse;
import mrkresnofatih.com.gitlab.treylleapi.services.users.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SimpleBrowseServiceTest {
    @Mock
    private UserService userService;
    private SimpleBrowseService browseService;

    @BeforeEach
    void setUp() {
        browseService = new SimpleBrowseService(userService);
    }

    @Test
    void searchUser_when_userList_fail_should_return_error() {
        var req = new BrowseUserSearchRequest("kevin.durant", "kevin.durant123", 25);
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>("failed to list users"));
        var result = browseService.searchUser(req);
        Assertions.assertThat(result.isError()).isTrue();
    }

    @Test
    void searchUser_when_userList_success_should_return_success() {
        var req = new BrowseUserSearchRequest("kevin.durant", "kevin.durant123", 25);
        var mockUserList = new ArrayList<UserGetResponse>();
        var mockUserResult = new UserGetResponse();
        mockUserResult.setUsername("kevin.durant1231");
        mockUserResult.setFullName("Kevin Durant");
        mockUserResult.setAvatarUrl("someAvatarUrl");
        mockUserResult.setBio("someBio");
        mockUserList.add(mockUserResult);
        Mockito.when(userService.list(Mockito.any(UserListRequest.class)))
                .thenReturn(new FuncResponse<>(new UserListResponse(mockUserList)));
        var result = browseService.searchUser(req);
        Assertions.assertThat(result.isError()).isFalse();
    }
}